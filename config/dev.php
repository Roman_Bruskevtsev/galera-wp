<?php
define( 'ENV', 'dev' );

define( 'DISALLOW_FILE_MODS', true );
define( 'DISALLOW_FILE_EDIT', true );
define( 'AUTOMATIC_UPDATER_DISABLED', true );
define( 'ALLOW_UNFILTERED_UPLOADS', true );

define( 'DB_NAME',       'dev_gal' );
define( 'DB_USER',       'gal_user' );
define( 'DB_PASSWORD',   'qxMqmRGz' );
define( 'DB_HOST',       'localhost' );

ini_set( 'display_errors', true );
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', true );

/** Enable W3 Total Cache */
define('WP_CACHE', true); 
define('WP_MEMORY_LIMIT', '128M');

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY',         '$kN=&}$OTPg^7GTYn@_l0a:0L8O |:7%IN0Zs$op l-191aN=FNGIXA,-,%<zRUI');
define('SECURE_AUTH_KEY',  '1(<pqvgw8.?0M:?E^:A2~zF*od)`|-!wQ7[TN.|E?qHg`4g[q-9yr/6Z]|Buisx7');
define('LOGGED_IN_KEY',    '-m2M-(^]Lh7RaS?Hrx0RXytey4!TQ?p8hSba@3]+8yBYnEIekvx[]0[WM)4&_@{1');
define('NONCE_KEY',        'M ++lY@e+$>/%3jZC5D|7+=N|#T:N?-zu08Y 1q+, e-+:UsxcNA(StZ56B9b0wi');
define('AUTH_SALT',        'NOD|W.ndW[rUyj2SX<E;KQ2ynyd$NW]KUF$Qx:$!<<C~:=|`V+BKN)-r-nPI=pi@');
define('SECURE_AUTH_SALT', 'O*[L-!==kj|+|xF efOJi-,r-|v9Y3Duh`5DLdZN.w9-0c^5,r|QC+E|1p&8hEe5');
define('LOGGED_IN_SALT',   '[Be5{u=//TYOT5Ng3&DI,?=d!?HAS7w:blVFmrcqvI8zz_5#g9@TV_uZD$|CkB@F');
define('NONCE_SALT',       'zUsH_XIl`e1]+uTF7zF7l#>::[yMkLR3T8@(R|n9LzK1S8[OScjdv l1uH;|iBF ');