<?php
/**
 *
 * @package WordPress
 * @subpackage Galera
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>
	<section class="error__page">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3"></div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<div class="image" data-aos="zoom-in" data-aos-duration="1500"></div>
				</div>
				<div class="col-lg-8">
					<div class="text" data-aos="fade-left" data-aos-duration="1000">
						<h1><?php _e('Oh no,', 'galera'); ?><br><?php _e('that’s not here…', 'galera'); ?></h1>
						<p><?php _e('Sorry, but we can’t find the page you were looking for.', 'galera'); ?></p>
						<a class="btn btn__white" href="<?php echo esc_url( home_url( '/' ) ); ?>"><span><?php _e('Go home', 'galera'); ?></span></a>
					</div>
					
				</div>
			</div>
		</div>
	</section>
<?php get_footer();