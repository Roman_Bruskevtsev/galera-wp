<?php
/**
 *
 * @package WordPress
 * @subpackage Galera
 * @since 1.0
 * @version 1.0
 */
get_header(); 

$galera = new GaleraClass();

get_template_part( 'template-parts/page/content-title', 'capabilities' ); ?>

<section class="posts__section">
	<div class="container">
		<div class="row">
			<?php echo $galera->get_capabilities(); ?>
		</div>
	</div>
</section>

<?php 
get_template_part( 'template-parts/capability/content', 'ready_talk' );
get_footer();