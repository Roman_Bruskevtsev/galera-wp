<?php
/**
 *
 * @package WordPress
 * @subpackage Galera
 * @since 1.0
 * @version 1.0
 */
get_header(); 

$galera = new GaleraClass();

get_template_part( 'template-parts/page/content-title', 'services' ); ?>

<section class="posts__section">
	<div class="container">
		<div class="row">
			<?php echo $galera->get_services(); ?>
		</div>
	</div>
</section>

<?php 
get_template_part( 'template-parts/service/content', 'ready_talk' );
get_footer();