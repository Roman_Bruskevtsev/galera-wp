'use strict';

(function($) { 
    $(document).ready(function() {

        if( $('.team__slider').length ){
            $('.team__slider').slick({
                infinite:       true,
                slidesToShow:   1,
                slidesToScroll: 1,
                dots:           true,
                arrows:         true,
                speed:          1000,
                autoplay:       false,
                autoplaySpeed:  5000
            });
        }

        if( $('.video__slider').length ){
            $('.video__slider').slick({
                infinite:       true,
                slidesToShow:   1,
                slidesToScroll: 1,
                dots:           true,
                arrows:         true,
                speed:          1000,
                autoplay:       false,
                autoplaySpeed:  5000
            });
        }

        if( $('.latest__slider').length ){
            $('.latest__slider').slick({
                infinite:       false,
                slidesToShow:   1,
                slidesToScroll: 1,
                dots:           false,
                appendArrows:   $('.latest__video__section .slider__nav'),
                speed:          1000,
                autoplay:       false,
                autoplaySpeed:  5000
            });
        }

        if( $('.images__slider').length ){
            $('.images__slider').slick({
                infinite:       false,
                slidesToShow:   1,
                slidesToScroll: 1,
                dots:           false,
                appendArrows:   $('.image__slider__section .slider__nav'),
                speed:          1000,
                autoplay:       false,
                autoplaySpeed:  5000
            });
        }

        if( $('.feedback__slider').length ){
            $('.feedback__slider').slick({
                infinite:       true,
                slidesToShow:   1,
                slidesToScroll: 1,
                dots:           false,
                appendArrows:   $('.feedback__slider__section .slider__nav'),
                speed:          500,
                autoplay:       false,
                fade:           true,
                autoplaySpeed:  5000
            });
        }
    });
    $(window).on('load', function(){
        $('.mobile__btn').on('click', function(){
            $(this).toggleClass('show');
            $('.mobile__menu').toggleClass('show');
            $('header').toggleClass('show__menu');
        });
        
        
        
        document.querySelectorAll('.before__after__section .images').forEach(function(element){
            new Cocoen(element);
        });

        var myLazyLoad = new LazyLoad({
            elements_selector: '.lazy',
            load_delay: 300
        });

        if( $('.bubbles__wrapper').length ){
            var scene = document.querySelector('.bubbles__wrapper');
            var parallaxInstance = new Parallax(scene);
        }
        
    	$('.paroller').paroller({
            factor: 0.1,
            type: 'foreground',
            direction: 'vertical',
            transition: 'transform 0.3s ease'
        });

    	if( $('.cases__slider').length ){
            let breakpointWidth = 991,
                setting = {
                    infinite:       true,
                    slidesToShow:   1,
                    slidesToScroll: 1,
                    fade:           true
                };
                
            // if( $('.cases__slider .slide').length > 1 ) setting['dots'] = true;

    		$('.cases__slider').slick(setting);

            if( $(window).width() <= breakpointWidth ) {
                $('.cases__slider').slick('unslick');
            } else if( !$('.cases__slider').hasClass('slick-initialized') ){
                $('.cases__slider').slick(setting);
            }

            $(window).on('resize', function(){
                if( $(window).width() <= breakpointWidth ) {
                    $('.cases__slider').slick('unslick');
                } else if( !$('.cases__slider').hasClass('slick-initialized') ){
                    $('.cases__slider').slick(setting);
                }
            });
    	}

        

        /*Select2*/
        $('select').select2({
            minimumResultsForSearch: -1,
            width: '100%'
        });

        $('.preloader__wrapper').addClass('hide');

        AOS.init();
    });
})(jQuery);