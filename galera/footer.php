<?php
/**
 *
 * @package WordPress
 * @subpackage Galera
 * @since 1.0
 * @version 1.0
 */
?>
	<?php 
	if( is_page() || is_singular('case') || is_singular( 'post' ) ){
		$footer_form = get_field('footer_form');
		switch ($footer_form) {
			case '0':
				
				break;
			case '1':
				get_template_part( 'template-parts/footer/contact', 'form' );
				break;
			case '2':
				get_template_part( 'template-parts/footer/content', 'newsletter' );
				break;
			default:
				
				break;
		}
	} ?>
    </main>
    <footer>
    	<?php if ( is_active_sidebar( 'footer-1' ) || 
    			   is_active_sidebar( 'footer-2' ) || 
    			   is_active_sidebar( 'footer-3' ) ||
    			   is_active_sidebar( 'footer-3' ) ) { ?>
    	<div class="widgets__line">
    		<div class="container">
				<div class="row">
					<?php if ( is_active_sidebar( 'footer-1' ) ) { ?>
						<div class="col-md-12 col-lg-4">
							<?php 
		                    $logo = get_field('logo', 'option');
		                    if( $logo ) { ?>
		                    <a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
		                        <img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
		                    </a>
		                    <?php }
							dynamic_sidebar( 'footer-1' ); 
							if( get_field('contact_us_page', 'option') ) { ?>
	                        <a href="<?php echo get_permalink( get_field('contact_us_page', 'option') ); ?>" class="btn btn__red"><span><?php _e('Contact us', 'galera'); ?></span></a>
	                        <?php } ?>	
						</div>
					<?php } ?>
					<?php if ( is_active_sidebar( 'footer-2' ) ) { ?>
						<div class="col-6 col-lg-3"><?php dynamic_sidebar( 'footer-2' ); ?></div>
					<?php } ?>
					<?php if ( is_active_sidebar( 'footer-3' ) ) { ?>
						<div class="col-6 col-lg-2"><?php dynamic_sidebar( 'footer-3' ); ?></div>
					<?php } ?>
					<?php if ( is_active_sidebar( 'footer-4' ) ) { ?>
						<div class="col-md-12 col-lg-3"><?php dynamic_sidebar( 'footer-4' ); ?></div>
					<?php } ?>
				</div>
			</div>
    	</div>
    	<?php } ?>
		<div class="footer__line">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<?php if( get_field('copyright_text', 'option') ) { ?>
						<div class="copyright d-none d-lg-block"><p><?php the_field('copyright_text', 'option'); ?></p></div>
						<?php } ?>
					</div>
					<div class="col-lg-6">
						<?php if( get_field('clutch', 'option') || 
								  get_field('dribbble', 'option') ||
								  get_field('behance', 'option') ||
								  get_field('facebook', 'option') ) { ?>
						<div class="social__block text-right">
							<ul>
								<?php if( get_field('clutch', 'option') ) { ?>
									<li><a href="<?php the_field('clutch', 'option'); ?>" target="_blank"><?php _e('Clutch', 'galera'); ?></a></li>
								<?php } ?>
								<?php if( get_field('dribbble', 'option') ) { ?>
									<li><a href="<?php the_field('dribbble', 'option'); ?>" target="_blank"><?php _e('Dribbble', 'galera'); ?></a></li>
								<?php } ?>
								<?php if( get_field('behance', 'option') ) { ?>
									<li><a href="<?php the_field('behance', 'option'); ?>" target="_blank"><?php _e('Behance', 'galera'); ?></a></li>
								<?php } ?>
								<?php if( get_field('facebook', 'option') ) { ?>
									<li><a href="<?php the_field('facebook', 'option'); ?>" target="_blank"><?php _e('Facebook', 'galera'); ?></a></li>
								<?php } ?>
							</ul>
						</div>
						<?php } ?>
						<?php if( get_field('copyright_text', 'option') ) { ?>
						<div class="copyright d-block d-lg-none"><p><?php the_field('copyright_text', 'option'); ?></p></div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
    </footer>
    <div class="popup__wrapper"></div>
    <?php get_template_part( 'template-parts/footer/popup', 'video' ); ?>
    <!-- <div class="cursor">
    	<div class="circle"></div>
    </div> -->
    <?php wp_footer(); ?>
</body>
</html>