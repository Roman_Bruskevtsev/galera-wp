<?php
/**
 *
 * @package WordPress
 * @subpackage Galera
 * @since 1.0
 * @version 1.0
 */

get_header(); 

get_template_part( 'template-parts/page/content', 'title' );

if( have_rows('content') ):

	while ( have_rows('content') ) : the_row();

		if( get_row_layout() == 'main_banner' ) :
			get_template_part( 'template-parts/page/main_banner' );
		elseif( get_row_layout() == 'services_section' ) :
			get_template_part( 'template-parts/page/services_section' );
		elseif( get_row_layout() == 'cases_section' ) :
			get_template_part( 'template-parts/page/cases_section' );
		elseif( get_row_layout() == 'team_section' ) :
			get_template_part( 'template-parts/page/team_section' );
		elseif( get_row_layout() == 'partners_section' ) :
			get_template_part( 'template-parts/page/partners_section' );
		elseif( get_row_layout() == 'banner_text_section' ) :
			get_template_part( 'template-parts/page/banner_text_section' );
		elseif( get_row_layout() == 'text_image_section' ) :
			get_template_part( 'template-parts/page/text_image_section' );
		elseif( get_row_layout() == 'image_text_section' ) :
			get_template_part( 'template-parts/page/image_text_section' );
		elseif( get_row_layout() == 'contact_form_section' ) :
			get_template_part( 'template-parts/page/contact_form_section' );
		elseif( get_row_layout() == 'character_development_section' ) :
			get_template_part( 'template-parts/page/character_development_section' );
		elseif( get_row_layout() == 'benefits_section' ) :
			get_template_part( 'template-parts/page/benefits_section' );
		elseif( get_row_layout() == 'process_section' ) :
			get_template_part( 'template-parts/page/process_section' );
		elseif( get_row_layout() == 'video_section' ) :
			get_template_part( 'template-parts/page/video_section' );
		elseif( get_row_layout() == 'text_section' ) :
			get_template_part( 'template-parts/page/text_section' );
		elseif( get_row_layout() == 'video_slider_section' ) :
			get_template_part( 'template-parts/page/video_slider_section' );
		elseif( get_row_layout() == 'videos_grid' ) :
			get_template_part( 'template-parts/page/videos_grid' );
		elseif( get_row_layout() == 'feedback_section' ) :
			get_template_part( 'template-parts/page/feedback_section' );
		elseif( get_row_layout() == 'full_width_video_section' ) :
			get_template_part( 'template-parts/page/full_width_video_section' );
		elseif( get_row_layout() == 'quotes_section' ) :
			get_template_part( 'template-parts/page/quotes_section' );
		elseif( get_row_layout() == 'use_cases_section' ) :
			get_template_part( 'template-parts/page/use_cases_section' );
		elseif( get_row_layout() == 'gradient_contact_section' ) :
			get_template_part( 'template-parts/page/gradient_contact_section' );
		elseif( get_row_layout() == 'how_it_can_help_section' ) :
			get_template_part( 'template-parts/page/how_it_can_help_section' );
		elseif( get_row_layout() == 'companies_section' ) :
			get_template_part( 'template-parts/page/companies_section' );
		elseif( get_row_layout() == 'faq_section' ) :
			get_template_part( 'template-parts/page/faq_section' );
		elseif( get_row_layout() == 'banner_image_section' ) :
			get_template_part( 'template-parts/page/banner_image_section' );
		elseif( get_row_layout() == 'awards_section' ) :
			get_template_part( 'template-parts/page/awards_section' );
		elseif( get_row_layout() == 'team_section_grid' ) :
			get_template_part( 'template-parts/page/team_section_grid' );
		elseif( get_row_layout() == 'latest_video_section' ) :
			get_template_part( 'template-parts/page/latest_video_section' );
		elseif( get_row_layout() == 'feedback_slider' ) :
			get_template_part( 'template-parts/page/feedback_slider_section' );
		elseif( get_row_layout() == 'ready_to_talk_section' ) :
			get_template_part( 'template-parts/page/ready_to_talk_section' );
		elseif( get_row_layout() == 'image_slider_section' ) :
			get_template_part( 'template-parts/page/image-slider-section' );	
		endif;
	endwhile;

endif;

get_footer();