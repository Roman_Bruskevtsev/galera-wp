<?php
/**
 *
 * @package WordPress
 * @subpackage Galera
 * @since 1.0
 * @version 1.0
 */
/*ACF Import*/
// require get_template_directory() . '/inc/acf-import.php';
/*Theme settings*/
require get_template_directory() . '/inc/classes/theme_settings.php';
require get_template_directory() . '/inc/classes/forms.php';
require get_template_directory() . '/inc/classes/galera.php';