<?php
/**
 *
 * @package WordPress
 * @subpackage Galera
 * @since 1.0
 * @version 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head();  ?>
</head>
<body <?php body_class('load'); ?>>
    <?php 
    get_template_part( 'template-parts/header/preloader' ); ?>
    <header data-aos="fade-down">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="nav__bar">
                        <?php 
                        $logo = get_field('logo', 'option');
                        if( $logo ) { ?>
                        <a class="logo float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                        </a>
                        <?php } 
                        if( has_nav_menu('main') ) { ?>
                        <div class="menu__block d-none d-lg-block float-right">
                            <?php wp_nav_menu( array(
                                'theme_location'        => 'main',
                                'container'             => 'nav',
                                'container_class'       => 'main__nav'
                            ) ); 
                            if( get_field('contact_us_page', 'option') ) { ?>
                            <a href="<?php echo get_permalink( get_field('contact_us_page', 'option') ); ?>" class="btn btn__white"><span><?php _e('Contact us', 'galera'); ?></span></a>
                            <?php } ?>
                        </div>
                        <div class="mobile__btn d-block d-lg-none">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <?php if( has_nav_menu('main') ) { ?>
    <div class="mobile__menu d-block d-lg-none">
        <?php wp_nav_menu( array(
            'theme_location'        => 'main',
            'container'             => 'nav',
            'container_class'       => 'main__nav'
        ) ); 
        if( get_field('contact_us_page', 'option') ) { ?>
        <a href="<?php echo get_permalink( get_field('contact_us_page', 'option') ); ?>" class="btn btn__red"><span><?php _e('Contact us', 'galera'); ?></span></a>
        <?php } ?>
    </div>
    <?php } ?>
    <main>