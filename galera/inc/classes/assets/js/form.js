'use strict';

class FormClass{
    constructor(){
        this.init();
    }

    init(){
        
    }

    checkField(field){
    	let fieldValue = field.value;

    	if(fieldValue){
    		field.classList.add('not__empty');
    	} else {
    		field.classList.remove('not__empty');
    	}
    }

    validation(form, option = ''){
    	let formData        = new FormData(),
            fields 			= form.querySelectorAll('.form__field'),
    		fieldsError		= form.querySelectorAll('.field__error'),
    		formResponse 	= form.querySelector('.form__response'),
    		securityField 	= form.querySelector('input[name*="form-security"]'),
    		submitBtn		= form.querySelector('button[type="submit"]'),
    		xhr     		= new XMLHttpRequest(),
            filesParam      = '',
    		fieldsArray		= {},
    		fieldArray, fieldGroup;

    	submitBtn.classList.add('load');
    	if(fieldsError.length){
    		fieldsError.forEach(function(field){
    			field.remove();
    		});
    	}
    	if(formResponse) formResponse.remove();

        fields.forEach(function(field, e){
            if(field.getAttribute('type') == 'file'){
                formData.append('file', field.files[0]);
            } else {
                fieldArray = {
                    name: '',
                    value: '',
                    required: false
                };
                fieldGroup = field.closest('.field__group');
                fieldArray.required = (field.required) ? true : false;
                fieldArray.value = field.value;
                fieldArray.name = field.getAttribute('name');
                field.classList.remove('error');
                if( fieldGroup ) fieldGroup.classList.remove('error');
                fieldsArray[e] = fieldArray;
            }
        });

        fieldsArray = JSON.stringify(fieldsArray);

        formData.append( 'action', 'form_validation' );
        formData.append( 'security', securityField.value );
        formData.append( 'fields', fieldsArray );

        xhr.open('POST', ajaxurl, true);
        xhr.onload = function () {
            let responseArray = JSON.parse(this.response),
                message       = responseArray.data.text,
                url			  = decodeURIComponent(responseArray.data.url),
                formSubmit    = form.querySelector('button[type="submit"]');

            if (this.status >= 200 && this.status < 400) {

                form.reset();

                formSubmit.insertAdjacentHTML('afterend', '<div class="form__success form__response">' + message + '</div>' );

                if( url ) window.open(url, '_self');

            } else if( this.status >= 400 && this.status < 500 ){
                let responseArray = JSON.parse(this.response),
                	fields        = responseArray.data,
                    field;

                fields.forEach(function(resField){
                    if(resField.error){
                        field = form.querySelector('[name="' + resField.name + '"]');
                        field.classList.add('error');
                        field.closest('.field__group').classList.add('error');
                        field.insertAdjacentHTML('afterend', '<span class="field__error">' + resField.error + '</span>' );
                    }
                });
            } else {
                formSubmit.insertAdjacentHTML('afterend', '<div class="form__error form__response">' + message + '</div>' );
            }
        }
        xhr.onerror = function() {
            console.log('connection error');
        };
        xhr.send(formData);

    	submitBtn.classList.remove('load');

    	return false;
    }
}

let formClass = new FormClass();