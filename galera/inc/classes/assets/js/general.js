'use strict';

class GeneralClass{
    constructor(){
        this.init();
    }

    init(){
        // this.mouseMove();
        this.mainVideoBlock();
    	this.videoBlocks();
    	this.headerBackground();
    	this.servicesAcordeon();
        this.slideImage();
        this.caseVideo();
        this.showVideoPopup();
        this.closeVideoPopup();
        this.latestWork();

        window.addEventListener('load', AOS.refresh);
    }

    mainVideoBlock(){
        let playBtns = document.querySelectorAll('.main__video .play');

        if(playBtns){
            playBtns.forEach(function(playBtn){
                playBtn.addEventListener('click', function(){
                    let videoBlock = this.closest('.main__video'),
                        iframe = videoBlock.querySelector('iframe'),
                        src = iframe.dataset.src;
                    iframe.setAttribute('src', src);

                    let player = new Vimeo.Player(iframe);
                    videoBlock.classList.add('play');
                    player.play();
                }, false);
            });
        }
    }

    caseVideo(){
        let playBtn = document.querySelector('.post__video .play');

        if(playBtn){
            playBtn.addEventListener('click', function(){
                let videoBlock = this.closest('.post__video'),
                    iframe = videoBlock.querySelector('iframe'),
                    src = iframe.dataset.src;
                iframe.setAttribute('src', src);

                let player = new Vimeo.Player(iframe);
                videoBlock.classList.add('play');
                player.play();
            }, false);
        }
    }

    mouseMove(){
        let body = document.querySelector('body'),
            cursor = body.querySelector('.cursor'),
            hoverElementArray = [
                'A',
                'BUTTON'
            ],
            mouseOverElement, mouseOverElementClasslist;

        body.addEventListener('mousemove', e => {
            mouseOverElement = e.target.tagName;
            mouseOverElementClasslist = e.target.classList;
            if( hoverElementArray.includes(mouseOverElement) || mouseOverElementClasslist.contains('cursor__hover') ){
                cursor.classList.add('hover');
            } else {
                cursor.classList.remove('hover');
            }
            cursor.setAttribute('style', 'top:' + e.pageY + 'px; left:' + e.pageX + 'px;');
        });
    }

    headerBackground(){
    	let scrollPosition,
    		header = document.querySelector('header');

    	window.addEventListener('scroll', function() {
    		scrollPosition = window.scrollY;

    		if( scrollPosition > 150 ){
    			header.classList.add('background');
    		} else {
    			header.classList.remove('background');
    		}
    	});
    }

    videoBlocks(){
    	let playBtns = document.querySelectorAll('.video__block .play');

        if(playBtns){
            playBtns.forEach(function(playBtn){
                playBtn.addEventListener('click', function(){
                    let videoBlock = this.closest('.video__block'),
                        iframe = videoBlock.querySelector('iframe'),
                        src = iframe.dataset.src;
                    iframe.setAttribute('src', src);

                    let player = new Vimeo.Player(iframe);
                    videoBlock.classList.add('play');
                    player.play();
                }, false);
            });
        }
    }

    servicesAcordeon(){
    	let servicesRow = document.querySelector('.services__section .services');

    	if( servicesRow ){
    		let services = servicesRow.querySelectorAll('.service'),
                servicesLength = services.length,
                element = 1,
                serviceWindowTopHeight,
    			serviceDocumentTopHeight,
    			serviceTitle,
    			serviceTitleLeftPosition,
                serviceVideo,
                serviceVideoWindowBottomHeight,
    			topHeight = 0,
                elementsHeight = 0,
                bottomHeight = 0,
    			elementTopHeight,
                elementBottomHeight,
                scrolling = true,
                windowScrollHeight,
                servicesWindowBottomHeight = servicesRow.scrollHeight + servicesRow.offsetTop,
                servicesScroll = servicesRow.offsetTop,
                serviceHeight, serviceTextHeight;
            
    		window.addEventListener('scroll', function() {
                windowScrollHeight = window.pageYOffset + window.innerHeight;
                if( servicesRow.getBoundingClientRect().top + servicesRow.scrollHeight - 678 <= 0 && scrolling ){
                    scrolling = false;
                    servicesRow.classList.add('absolute');

                    for (var i = servicesLength - 1; i >= 0; i--) {
                        serviceTitle = services[i].querySelector('.name');
                        serviceHeight = serviceTitle.getBoundingClientRect().height;
                        serviceTextHeight = i >= 1 ? services[i - 1].querySelector('.content').getBoundingClientRect().height : 0;

                        serviceTitle.setAttribute('style', 'bottom:' + (bottomHeight) + 'px;');
                        
                        bottomHeight =  bottomHeight + serviceHeight - serviceTextHeight;
                    }
                } else if ( servicesRow.getBoundingClientRect().top + servicesRow.scrollHeight - 678 > 0 && !scrolling ) {
                    element = 0;
                    elementsHeight = 0;
                    bottomHeight = 0;
                    scrolling = true;
                    servicesRow.classList.remove('absolute');
                    services.forEach( function(service){
                        serviceTitle = service.querySelector('.name');
                        serviceVideo = service.querySelector('.video');
                        serviceTitleLeftPosition = serviceTitle.getBoundingClientRect().left;
                        serviceTitle.classList.remove('fixed');
                        serviceVideo.classList.remove('show');
                    });
                }
                if( scrolling ) {
                    services.forEach( function(service){
                        serviceWindowTopHeight = service.getBoundingClientRect().top;
                        serviceDocumentTopHeight = service.scrollHeight;
                        serviceTitle = service.querySelector('.name');
                        serviceTitleLeftPosition = serviceTitle.getBoundingClientRect().left;
                        serviceVideo = service.querySelector('.video');
                        serviceVideoWindowBottomHeight = serviceVideo.getBoundingClientRect().bottom;
                        elementTopHeight = parseInt(service.dataset.top) + 30;

                        if( serviceWindowTopHeight < topHeight + elementTopHeight ){
                            serviceTitle.classList.add('fixed');
                            serviceTitle.setAttribute('style', 'top:' + elementTopHeight + 'px; left:' + serviceTitleLeftPosition + 'px;');
                        } else {
                            serviceTitle.classList.remove('fixed');
                            serviceTitle.removeAttribute('style');
                        }
                        if( serviceVideoWindowBottomHeight < 1100 ){
                            serviceVideo.classList.add('show');
                            serviceVideo.querySelector('video').play();
                        } else {
                            serviceVideo.classList.remove('show');
                        }
                    });
                }
    		});
    	}
    }

    slideImage(){
        let images = document.querySelectorAll('.image__slide');

        if( images ){
            let scrolling = true,
                imageWindowBottomHeight;
            window.addEventListener('scroll', function() {
                images.forEach( function(image){
                    imageWindowBottomHeight = image.getBoundingClientRect().bottom;
                    if( imageWindowBottomHeight < 1000 ){
                        image.classList.add('show');
                    } else {
                        image.classList.remove('show');
                    }
                });
            });
        }
    }

    showAward(button){
    	let ribbons = button.closest('ul').querySelectorAll('li'),
    		texts = button.closest('.awards__block').querySelectorAll('.description'),
    		indexRibbon = Array.prototype.slice.call(ribbons).indexOf(button, 0);

        ribbons.forEach(function(ribbon){
            ribbon.classList.remove('active');
        });

        texts.forEach(function(text){
            text.classList.remove('active');
        });

    	texts[indexRibbon].classList.add('active');
    	button.classList.add('active');
    }

    showTeamPerson(person){
        let teamList    = Array.prototype.slice.call(person.closest('.team__information').children),
            imageList   = person.closest('.team__block').querySelectorAll('.team__list .image'),
            indexPerson = teamList.indexOf(person);
        
        imageList[indexPerson].classList.add('hover');
    }

    hideTeamPerson(person){
        let teamList    = Array.prototype.slice.call(person.closest('.team__information').children),
            imageList   = person.closest('.team__block').querySelectorAll('.team__list .image'),
            indexPerson = teamList.indexOf(person);

        imageList[indexPerson].classList.remove('hover');
    }

    playVideo(url, button){
        let popup = button.closest('.video__slider__section').querySelector('.video__popup'),
            videoBlock = popup.querySelector('.video'),
            playerOption = {
                url: url
            };

        var videoPlayer = new Vimeo.Player(videoBlock, playerOption);
        videoPlayer.play();
        popup.classList.add('show');
    }

    playCellVideo(url, button){
        let popup = button.closest('.videos__grid').querySelector('.video__popup'),
            videoBlock = popup.querySelector('.video'),
            playerOption = {
                url: url
            };

        var videoPlayer = new Vimeo.Player(videoBlock, playerOption);
        videoPlayer.play();

        popup.classList.add('show');
    }

    closeVideo(button){
        let popup = button.closest('.video__slider__section').querySelector('.video__popup'),
            videoBlock = popup.querySelector('.video'),
            player;

        popup.classList.remove('show'); 
        player = new Vimeo.Player(videoBlock);
        player.destroy();
    }

    closeCellVideo(button){
        let popup = button.closest('.videos__grid').querySelector('.video__popup'),
            videoBlock = popup.querySelector('.video'),
            player;
        popup.classList.remove('show');  
        player = new Vimeo.Player(videoBlock);
        player.destroy();
    }

    showVideoPopup(){
        let showVideoBtns = document.querySelectorAll('.show__video'),
            popupWrapper = document.querySelector('.popup__wrapper'),
            popupBlock = document.querySelector('.popup__video'),
            popupIframe = popupBlock.querySelector('iframe');

        if(showVideoBtns){
            showVideoBtns.forEach(function(playBtn){
                playBtn.addEventListener('click', function(){
                    let src = playBtn.dataset.src;
                    popupIframe.setAttribute('src', src);

                    let player = new Vimeo.Player(popupIframe);
                    popupWrapper.classList.add('show');
                    popupBlock.classList.add('show');
                    player.play();
                }, false);
            });
        }
    }

    closeVideoPopup(){
        let closeVideoBtns = document.querySelectorAll('.popup__video .close'),
            popupWrapper = document.querySelector('.popup__wrapper'),
            popupBlock = document.querySelector('.popup__video'),
            popupIframe = popupBlock.querySelector('iframe');

        if(closeVideoBtns){
            closeVideoBtns.forEach(function(closeBtn){
                closeBtn.addEventListener('click', function(){
                    let player = new Vimeo.Player(popupIframe);
                    popupWrapper.classList.remove('show');
                    popupBlock.classList.remove('show');
                    player.pause();
                }, false);
            });
        }
    }

    latestWork(){
        let projects = document.querySelectorAll('.project__block'),
            closeButtons = document.querySelectorAll('.latest__popups .close'),
            workNumber, popups, iframe, videoUrl;

        if( projects ){
            projects.forEach(function(project){
                project.addEventListener('click', function(){
                    workNumber = parseInt(project.dataset.id);
                    popups = project.closest('.latest__work__section').nextElementSibling.querySelectorAll('.popup');
                    iframe = popups[workNumber].querySelector('iframe');
                    if( iframe ) videoUrl = iframe.dataset.src;

                    popups[workNumber].classList.add('show');
                    if( iframe ) iframe.setAttribute('src', videoUrl);
                });
            });
        }

        if( closeButtons ){
            closeButtons.forEach(function(close){
                close.addEventListener('click', function(){
                    popups.forEach(function(popup){
                        popup.classList.remove('show');
                    });
                });
            });
        }
    }
}

let galera = new GeneralClass();