<?php
class FormsClass {
	public function __construct(){
		$this->scriptsDir = get_theme_file_uri().'/inc/classes/assets/js';
        $this->stylesDir = get_theme_file_uri().'/inc/classes/assets/css';

		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_ajax_form_validation', array( $this, 'form_validation' ) );
        add_action( 'wp_ajax_nopriv_form_validation', array( $this, 'form_validation' ) );

        add_filter( 'wp_mail_from_name', array( $this, 'email_sender_name' ) );
        add_filter( 'wp_mail_from', array( $this, 'sender_email' ) );
	}

	public function email_sender_name($original_email_from){
		return get_bloginfo('name');
	}

	public function sender_email(){
		return 'hello@galera.agency';
	}

	public function email_header($width = 960){
		$output = '
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            <html>
            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="profile" href="http://gmpg.org/xfn/11">
            </head>
            <body style="color: rgba(255,255,255,0.7); font-size: 16px; padding: 0; margin: 0; font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif; background-color: #fff;">
                <table width="'.$width.'" cellspacing="0" cellpadding="0" border="0" style="position: relative; padding: 40px 120px 80px 120px; background-color: #010001; margin: 80px auto 0px;">
                    <thead>
                        <tr>
                            <th>
                                <a href="https://galera.agency" target="_blank" style="display: block; text-align: left;">
                                    <img src="'.get_theme_file_uri( '/assets/images/logo.png' ).'" width="107" height="28" alt="'.get_bloginfo('name').'" style="margin: 0 0 84px;">
                                </a>
                            </th>
                            <th style="vertical-align: top;">
                            	<h6 style="color: rgba(255,255,255,0.7); margin: 0; font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif; font-weight: 400; font-size: 16px; line-height: 32px; text-align: right; letter-spacing: 0.27px;">'.__('Motion graphics design & animation agency', 'galera').'</h6>
                            </th>
                        </tr>
                    </thead>';

        return $output;
	}

	public function customer_email_body(){
		$text = get_field('form_message_text', 'option');
		$output = '	<tbody>
				    	<tr>
				    		<td colspan="2">
				    			<h1 style="color: #fff; font-size: 36px; font-weight: bold; line-height: 48px; margin-top: 0px; margin-bottom: 20px">'.$text['title'].'</h1>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td colspan="2">
				    			<p style="font-size: 24px; color: rgba(255,255,255,0.7); letter-spacing: 0.4px; line-height: 40px; margin-bottom: 40px;">'.$text['text'].'</p>
				    		</td>
				    	</tr>
					</tbody>';

		return $output;
	}

	public function admin_email_body($fields){
		$output = '	<tbody>
				    	<tr>
				    		<td colspan="2">
				    			<h1 style="color: #fff; font-size: 36px; font-weight: bold; line-height: 48px; margin-top: 0px; margin-bottom: 20px">'.__('We received a new request!', 'galera').'</h1>
				    		</td>
				    	</tr>
				    	<tr>
				    		<td colspan="2">
				    			<p style="font-size: 24px; color: rgba(255,255,255,0.7); letter-spacing: 0.4px; line-height: 40px; margin-bottom: 40px;">'.__('Form details:', 'galera').'</p>
				    		</td>
				    	</tr>';
		foreach ($fields as $field) {
			if( !empty($field->value) && ($field->name !='admin-email') ){
				if( $field->name == 'file' ){
					$output .= '<tr>
					    		<td>
					    			<p style="font-size: 16px; color: rgba(255,255,255,0.7); line-height: 24px; margin: 0 0 15px 0;">'.$field->name.':</p>
				    			</td>
					    		<td>
					    			<a href="'.$field->url.'" target="_blank" style="font-size: 16px; color: color: rgba(255,255,255,0.7); line-height: 24px; margin: 0 0 15px 0;">'.$field->value.'</a>
					    		</td>
					    	</tr>';
				} else {
					$output .= '<tr>
					    		<td>
					    			<p style="font-size: 16px; color: color: rgba(255,255,255,0.7); line-height: 24px; margin: 0 0 15px 0;">'.$field->name.':</p>
				    			</td>
					    		<td style="font-size: 16px; color: color: rgba(255,255,255,0.7); line-height: 24px; margin: 0 0 15px 0;">
					    			<p>'.$field->value.'</p>
					    		</td>
					    	</tr>';
				}
			}
		}
		$output .='	</tbody>';

		return $output;
	}

	public function email_footer($width = 960){
		$output = '
	            </table>
	            <table width="'.$width.'" cellspacing="0" cellpadding="0" border="0" style="position: relative; padding: 18px 120px 22px 120px; background-color: #0D0D0D; margin: 0 auto 120px;">
	            	<tfoot>
            			<tr style="varticla-align: top;">
            				<td colspan="1" style="padding: 0;">
            					<h3 style="color: #fff; margin: 0; font-size: 24px; font-weight: bold; line-height: 32px;">'.__('Follow us:', 'galera').'</h3>
            				</td>
            				<td colspan="3" style="padding: 0;">
            					<ul style="margin: 0; padding: 0; list-style: none; width: 100%; text-align: right;">
            						<li style="display: inline-block; margin: 0 0 0 0;">
            							<a style="color: rgba(255,255,255,0.7); font-size: 16px; line-height: 32px; letter-spacing: 0.27px; text-decoration: none;" href="https://clutch.co/profile/galera" target="_blank">'.__('Clutch', 'galera').'</a>
            						</li>
            						<li style="display: inline-block; margin: 0 0 0 40px;">
            							<a style="color: rgba(255,255,255,0.7); font-size: 16px; line-height: 32px; letter-spacing: 0.27px; text-decoration: none;" href="https://dribbble.com/agencygalera" target="_blank">'.__('Dribbble', 'galera').'</a>
            						</li>
            						<li style="display: inline-block; margin: 0 0 0 40px;">
            							<a style="color: rgba(255,255,255,0.7); font-size: 16px; line-height: 32px; letter-spacing: 0.27px; text-decoration: none;" href="https://www.behance.net/Galera" target="_blank">'.__('Behance', 'galera').'</a>
            						</li>
            						<li style="display: inline-block; margin: 0 0 0 40px;">
            							<a style="color: rgba(255,255,255,0.7); font-size: 16px; line-height: 32px; letter-spacing: 0.27px; text-decoration: none;" href="https://www.facebook.com/agencygalera/" target="_blank">'.__('Facebook', 'galera').'</a>
            						</li>
            					</ul>
            				</td>
            			</tr>
            		</tfoot>
	           	</table>
	        </body>
	        </html>';
        return $output;
    }

	public function phone_validation($phone){
		$filtered_phone_number = filter_var( $phone, FILTER_SANITIZE_NUMBER_INT );
		$phone_to_check = str_replace('-', '', $filtered_phone_number);

		if (strlen($phone_to_check) < 10 || strlen($phone_to_check) > 14) {
	        return false;
	     } else {
	       return true;
	     }
	}

	public function form_validation(){
		if( wp_verify_nonce( $_POST['security'], 'security' ) ) {
			$fields = json_decode(stripslashes($_POST['fields']));
			$fieldsArray = [];
			$errors = false;
			$admin_email = get_option('admin_email');
			$user_email = '';
			foreach ($fields as $field) {
				$fieldArray = $field;
				$fieldArray->error = false;
				if ( $field->required ){
					if( $field->value == '' ){
						$fieldArray->error = __('This field is required, please fill it.', 'galera');
						$errors = true;
					} else {
						if( $field->name == 'email' ){
							if( !filter_var( $field->value, FILTER_VALIDATE_EMAIL ) ){
								$fieldArray->error = __('Email is wrong', 'galera');
								$errors = true;
							} else {
								$user_email = $field->value;
							}
						} elseif ( $field->name == 'phone' ){
							if( !$this->phone_validation($field->value) ){
								$fieldArray->error = __('Phone is wrong', 'galera');
								$errors = true;
							}
						} elseif( $field->name == 'admin-email' ){
							$admin_email = $field->value;
						}
					}
				} else {
					if( $field->value != '' ){
						if( $field->name == 'email' ){
							if( !filter_var( $field->value, FILTER_VALIDATE_EMAIL ) ){
								$fieldArray->error = __('Email is wrong', 'galera');
								$errors = true;
							} else {
								$user_email = $field->value;
							}
						} elseif ( $field->name == 'phone' ){
							if( !$this->phone_validation($field->value) ){
								$fieldArray->error = __('Phone is wrong', 'galera');
								$errors = true;
							}
						} elseif( $field->name == 'admin-email' ){
							$admin_email = $field->value;
						} elseif( $field->name == 'bot-checker' ){
							wp_send_json_error( __('Bot error!', 'galera'), 400 );
						}
					}
				}

				$fieldsArray[] = $fieldArray;
			}

			if( $errors ) {
				wp_send_json_error( $fieldsArray, 400 );
			} else {

				if( isset($_FILES['file']) ){
					if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );  
					$uploads = wp_upload_dir();

					$folderPath = ABSPATH . 'wp-content/uploads/files';
	                if (!file_exists($folderPath)) {
	                    mkdir($folderPath, 0777, true);
	                }
	                $ext = end((explode('.', $_FILES['file']['name']))); 
	                $name = explode('.', $_FILES['file']['name'])[0];

	                if( $ext== 'pdf' || $ext== 'docx' || $ext== 'doc' ){
	                	$fileName = $name . '_'.time(). '.' . $ext;
	                    $userTempData = $folderPath . '/' . $fileName;

	     				$uploadFile = move_uploaded_file($_FILES['file']['tmp_name'], $userTempData);

	     				if($uploadFile){
	     					$filePath = esc_url( home_url( '/' ) ).'wp-content/uploads/files/' . $fileName;

	     					$fieldsArray[] = (object) array(
			                	'name'		=> 'file',
			                	'value'		=> $fileName,
			                	'url'		=> $filePath
			                );
	     				}
	                }
				}

				$adminHeaders = $userHeaders = "Content-type: text/html; charset=utf-8 \r\n";  
                $adminHeaders .= __('From', 'galera').': info@bcareagency.com' . "\r\n";
                $userHeaders  .= __('From', 'galera').': info@bcareagency.com' . "\r\n";

				$adminMessage = $this->email_header().$this->admin_email_body($fieldsArray).$this->email_footer();
				if( $user_email != '' ) $userMessage = $this->email_header().$this->customer_email_body().$this->email_footer();

				$adminMail = wp_mail( $admin_email, 'Galera', $adminMessage, $adminHeaders );
				if( $user_email != '' ) $userMail = wp_mail( $user_email, 'Galera', $userMessage, $userHeaders );

				if( $adminMail && $userMail ) {
					$success_page = get_field('success_page', 'option');
					$success_url = $success_page ? get_the_permalink( $success_page ) : '';
					$data = array(
						'text' => __('Your request has been sent', 'galera'),
						'url'  => urlencode($success_url)
					);
					wp_send_json_success( $data, 200 );
				} else {
					wp_send_json_error( __('Server error, please try again later', 'galera'), 500 );
				}
			}
		}
	}
}

$forms = new FormsClass();