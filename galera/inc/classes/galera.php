<?php 

class GaleraClass {
	public function __construct(){
		$this->actions_init();
	}

	public function actions_init(){

	}

	public function __return_false() {
        return false;
    }

    public function get_posts($post_per_page = 7){
    	$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    	$output = '';
    	$args = array(
			'post_type'			=> 'post',
			'post_status'		=> 'publish',
			'paged' 			=> $paged
		);

		$query = new WP_Query( $args );

		if ( $query->have_posts() ) {
			$i = 0;
			
			while ( $query->have_posts() ) { $query->the_post();
				if($i == 0){
					$output .= get_template_part( 'template-parts/post/content', 'wide' );
				} else {
					$output .= get_template_part( 'template-parts/post/content', 'normal' );
				}
			$i++; }
		}
		wp_reset_postdata();
		if( $query->max_num_pages > 1 ){
			$output .= '<div class="col-lg-12">';
				$output .= '<div class="pagination text-center" data-aos="fade-up">';
					$output .= paginate_links( array(
				            'format'  		  => 'page/%#%',
				            'current' 		  => $paged,
				            'total'   		  => $query->max_num_pages,
				            'mid_size'        => 2,
				            'prev_next'       => true,
				            'prev_text'    	  => '<span class="prev__arrow"></span>',
				            'next_text'    	  => '<span class="next__arrow"></span>',
				            'show_all'		  => true
				        )); 
				$output .= '</div>';
			$output .= '</div>';
		}

		return $output;
    }

    public function get_cases($post_per_page = 7){
    	$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    	$output = '';
    	$args = array(
			'post_type'			=> 'case',
			'post_status'		=> 'publish',
			'paged' 			=> $paged
		);

		$query = new WP_Query( $args );

		if ( $query->have_posts() ) {
			$i = 0;
			
			while ( $query->have_posts() ) { $query->the_post();
				if($i == 0){
					$output .= get_template_part( 'template-parts/post/content', 'wide' );
				} else {
					$output .= get_template_part( 'template-parts/case/content', 'normal' );
				}
			$i++; }
		}
		wp_reset_postdata();
		if( $query->max_num_pages > 1 ){
			$output .= '<div class="col-lg-12">';
				$output .= '<div class="pagination text-center" data-aos="fade-up">';
					$output .= paginate_links( array(
				            'format'  		  => 'page/%#%',
				            'current' 		  => $paged,
				            'total'   		  => $query->max_num_pages,
				            'mid_size'        => 2,
				            'prev_next'       => true,
				            'prev_text'    	  => '<span class="prev__arrow"></span>',
				            'next_text'    	  => '<span class="next__arrow"></span>',
				            'show_all'		  => true
				        )); 
				$output .= '</div>';
			$output .= '</div>';
		}

		return $output;
    }

    public function get_services($post_per_page = 7){
    	$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    	$output = '';
    	$args = array(
			'post_type'			=> 'service',
			'post_status'		=> 'publish',
			'paged' 			=> $paged
		);

		$query = new WP_Query( $args );

		if ( $query->have_posts() ) {
			
			while ( $query->have_posts() ) { $query->the_post();
					$output .= get_template_part( 'template-parts/service/content', 'normal' );
			}
		}
		wp_reset_postdata();
		if( $query->max_num_pages > 1 ){
			$output .= '<div class="col-lg-12">';
				$output .= '<div class="pagination text-center" data-aos="fade-up">';
					$output .= paginate_links( array(
				            'format'  		  => 'page/%#%',
				            'current' 		  => $paged,
				            'total'   		  => $query->max_num_pages,
				            'mid_size'        => 2,
				            'prev_next'       => true,
				            'prev_text'    	  => '<span class="prev__arrow"></span>',
				            'next_text'    	  => '<span class="next__arrow"></span>',
				            'show_all'		  => true
				        )); 
				$output .= '</div>';
			$output .= '</div>';
		}

		return $output;
    }

    public function get_capabilities($post_per_page = 7){
    	$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    	$output = '';
    	$args = array(
			'post_type'			=> 'capability',
			'post_status'		=> 'publish',
			'paged' 			=> $paged
		);

		$query = new WP_Query( $args );

		if ( $query->have_posts() ) {
			
			while ( $query->have_posts() ) { $query->the_post();
					$output .= get_template_part( 'template-parts/capability/content', 'normal' );
			}
		}
		wp_reset_postdata();
		if( $query->max_num_pages > 1 ){
			$output .= '<div class="col-lg-12">';
				$output .= '<div class="pagination text-center" data-aos="fade-up">';
					$output .= paginate_links( array(
				            'format'  		  => 'page/%#%',
				            'current' 		  => $paged,
				            'total'   		  => $query->max_num_pages,
				            'mid_size'        => 2,
				            'prev_next'       => true,
				            'prev_text'    	  => '<span class="prev__arrow"></span>',
				            'next_text'    	  => '<span class="next__arrow"></span>',
				            'show_all'		  => true
				        )); 
				$output .= '</div>';
			$output .= '</div>';
		}

		return $output;
    }
}

$galera = new GaleraClass();