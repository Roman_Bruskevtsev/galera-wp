<?php

class ThemeSettingsClass {
	const SCRIPTS_VERSION = '1.0.55';

	public function __construct(){
		$this->scriptsDir = get_theme_file_uri().'/assets/js';
        $this->stylesDir = get_theme_file_uri().'/assets/css';

		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts_styles' ) );
		add_action( 'after_setup_theme', array( $this, 'theme_setup' ) );
		add_action( 'wp_footer',  array( $this, 'js_variables' ) );
		add_action( 'widgets_init', array( $this, 'widgets_init') );
		add_action( 'init', array( $this, 'custom_posts_init') );
		add_action( 'wp_head', array( $this, 'google_tag_manager') );
		// add_action( 'wp_head', array( $this, 'schema_org') );
		add_action( 'wp_body_open', array( $this, 'google_tag_manager_noscript') );


		add_filter( 'upload_mimes', array( $this, 'enable_svg_types' ), 99 );
		add_filter( 'wpseo_json_ld_output', '__return_false' );
		add_filter( 'body_class', array( $this, 'page_body_class' ) );
		add_filter( 'use_block_editor_for_post', '__return_false', 10 );
		add_filter( 'nav_menu_css_class', array( $this, 'wpdev_nav_classes' ), 10, 2 );
		add_filter( 'script_loader_tag', array( $this, 'add_async_attribute' ), 10, 2 );
	}

	public function scripts_styles() {
		wp_enqueue_style( 'galera-css', $this->stylesDir.'/main.min.css' , '', self::SCRIPTS_VERSION);
    	wp_enqueue_style( 'galera-style', get_stylesheet_uri() );

    	wp_enqueue_script( 'all-js', $this->scriptsDir.'/all.min.js', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    	wp_enqueue_script( 'classes-js', get_theme_file_uri().'/inc/classes/assets/js/classes.min.js', array( 'jquery' ), self::SCRIPTS_VERSION, true );

    	if( get_field('sharethis_api_key', 'option') && is_single() ) wp_enqueue_script( 'share-js', 'https://platform-api.sharethis.com/js/sharethis.js#property='.get_field('sharethis_api_key', 'option').'&product=custom-share-buttons&cms=sop', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    	if( is_page() || is_single() ) wp_enqueue_script( 'vimeo-js', 'https://player.vimeo.com/api/player.js', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    }

    public function theme_setup(){
    	// if( ENV == 'dev' ) update_option( 'upload_url_path', 'https://stage.bcareagency.com/wp-content/uploads', true );

    	load_theme_textdomain( 'galera' );
	    add_theme_support( 'automatic-feed-links' );
	    add_theme_support( 'title-tag' );
	    add_theme_support( 'post-thumbnails' );

	    add_image_size( 'service-image', 700, 394, true );
	    add_image_size( 'cases-image', 540, 304, true );
	    add_image_size( 'post-thumbnail', 460, 460, true );
	    add_image_size( 'post-thumbnail-wide', 940, 460, true );

	    register_nav_menus( array(
	        'main'          	=> __( 'Main Menu', 'galera' )
	    ) );

	    if( function_exists('acf_add_options_page') ) {
		    $general = acf_add_options_page(array(
		        'page_title'    => __('Theme General Settings', 'galera'),
		        'menu_title'    => __('Theme Settings', 'galera'),
		        'redirect'      => false,
		        'capability'    => 'edit_posts',
		        'menu_slug'     => 'theme-settings',
		    ));
		}

		add_post_type_support( 'page', 'excerpt' );
    }

    public function widgets_init(){
    	register_sidebar( array(
	        'name'          => __( 'Footer 1 (company information)', 'galera' ),
	        'id'            => 'footer-1',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'galera' ),
	        'before_widget' => '<div id="%1$s" class="widget %2$s about__widget">',
	        'after_widget'  => '</div>',
	        'before_title'  => '<h5><b>',
	        'after_title'   => '</b></h5>',
	    ) );
	    register_sidebar( array(
	        'name'          => __( 'Footer 2 (vertical menu)', 'galera' ),
	        'id'            => 'footer-2',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'galera' ),
	        'before_widget' => '<div id="%1$s" class="widget %2$s padding">',
	        'after_widget'  => '</div>',
	        'before_title'  => '<h5><b>',
	        'after_title'   => '</b></h5>',
	    ) );
	    register_sidebar( array(
	        'name'          => __( 'Footer 3 (vertical menu)', 'galera' ),
	        'id'            => 'footer-3',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'galera' ),
	        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	        'after_widget'  => '</div>',
	        'before_title'  => '<h5><b>',
	        'after_title'   => '</b></h5>',
	    ) );
	    register_sidebar( array(
	        'name'          => __( 'Footer 4 (contact block)', 'galera' ),
	        'id'            => 'footer-4',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'galera' ),
	        'before_widget' => '<div id="%1$s" class="widget %2$s contact__widget">',
	        'after_widget'  => '</div>',
	        'before_title'  => '<h5><b>',
	        'after_title'   => '</b></h5>',
	    ) );
    }

    public function custom_posts_init(){
		$post_labels = array(
			'name'					=> __('Cases', 'galera'),
			'singular_name'			=> __('Case', 'galera'),
			'add_new'				=> __('Add Case', 'galera'),
			'add_new_item'			=> __('Add New Case', 'galera'),
			'edit_item'				=> __('Edit Case', 'galera'),
			'new_item'				=> __('New Case', 'galera'),
			'view_item'				=> __('View Case', 'galera')
		);

		$post_args = array(
			'label'               	=> __('Cases', 'galera'),
			'description'        	=> __('Case information page', 'galera'),
			'labels'              	=> $post_labels,
			'supports'            	=> array( 'title', 'thumbnail'),
			'taxonomies'          	=> array( '' ),
			'hierarchical'       	=> false,
			'public'              	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'has_archive'         	=> true,
			'can_export'          	=> true,
			'show_in_nav_menus'   	=> true,
			'publicly_queryable'  	=> true,
			'exclude_from_search' 	=> false,
			'query_var'           	=> true,
			'capability_type'     	=> 'post',
			'menu_position'			=> 3,
			'rewrite'				=> array(
				'slug'				=> 'cases'
			),
			'menu_icon'           	=> 'dashicons-video-alt2'
		);
		register_post_type( 'case', $post_args );

		$post_labels = array(
			'name'					=> __('Services', 'galera'),
			'singular_name'			=> __('Service', 'galera'),
			'add_new'				=> __('Add Service', 'galera'),
			'add_new_item'			=> __('Add New Service', 'galera'),
			'edit_item'				=> __('Edit Service', 'galera'),
			'new_item'				=> __('New Service', 'galera'),
			'view_item'				=> __('View Service', 'galera')
		);

		$post_args = array(
			'label'               	=> __('Services', 'galera'),
			'description'        	=> __('Service information page', 'galera'),
			'labels'              	=> $post_labels,
			'supports'            	=> array( 'title', 'thumbnail', 'excerpt'),
			'taxonomies'          	=> array( '' ),
			'hierarchical'       	=> false,
			'public'              	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'has_archive'         	=> true,
			'can_export'          	=> true,
			'show_in_nav_menus'   	=> true,
			'publicly_queryable'  	=> true,
			'exclude_from_search' 	=> false,
			'query_var'           	=> true,
			'capability_type'     	=> 'post',
			'menu_position'			=> 3,
			'rewrite'				=> array(
				'slug'				=> 'services'
			),
			'menu_icon'           	=> 'dashicons-admin-tools'
		);
		register_post_type( 'service', $post_args );

		$post_labels = array(
			'name'					=> __('Capabilities', 'galera'),
			'singular_name'			=> __('Capability', 'galera'),
			'add_new'				=> __('Add Capability', 'galera'),
			'add_new_item'			=> __('Add New Capability', 'galera'),
			'edit_item'				=> __('Edit Capability', 'galera'),
			'new_item'				=> __('New Capability', 'galera'),
			'view_item'				=> __('View Capability', 'galera')
		);

		$post_args = array(
			'label'               	=> __('Capabilities', 'galera'),
			'description'        	=> __('Capability information page', 'galera'),
			'labels'              	=> $post_labels,
			'supports'            	=> array( 'title', 'thumbnail', 'excerpt'),
			'taxonomies'          	=> array( '' ),
			'hierarchical'       	=> false,
			'public'              	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'has_archive'         	=> true,
			'can_export'          	=> true,
			'show_in_nav_menus'   	=> true,
			'publicly_queryable'  	=> true,
			'exclude_from_search' 	=> false,
			'query_var'           	=> true,
			'capability_type'     	=> 'post',
			'menu_position'			=> 3,
			'rewrite'				=> array(
				'slug'				=> 'capabilities'
			),
			'menu_icon'           	=> 'dashicons-image-filter'
		);
		register_post_type( 'capability', $post_args );

		$post_labels = array(
			'name'					=> __('Projects', 'galera'),
			'singular_name'			=> __('Project', 'galera'),
			'add_new'				=> __('Add Project', 'galera'),
			'add_new_item'			=> __('Add New Project', 'galera'),
			'edit_item'				=> __('Edit Project', 'galera'),
			'new_item'				=> __('New Project', 'galera'),
			'view_item'				=> __('View Project', 'galera')
		);

		$post_args = array(
			'label'               	=> __('Projects', 'galera'),
			'description'        	=> __('Project information page', 'galera'),
			'labels'              	=> $post_labels,
			'supports'            	=> array( 'title', 'thumbnail', 'excerpt'),
			'taxonomies'          	=> array( '' ),
			'hierarchical'       	=> false,
			'public'              	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'has_archive'         	=> false,
			'can_export'          	=> true,
			'show_in_nav_menus'   	=> true,
			'publicly_queryable'  	=> false,
			'exclude_from_search' 	=> false,
			'query_var'           	=> true,
			'capability_type'     	=> 'post',
			'menu_position'			=> 3,
			'rewrite'				=> array(
				'slug'				=> 'projects'
			),
			'menu_icon'           	=> 'dashicons-video-alt3'
		);
		register_post_type( 'project', $post_args );
    }

    public function custom_taxonomy_init(){
		$taxonomy_labels = array(
			'name'                        => _x('Vacancies categories', 'galera'),
			'singular_name'               => _x('Vacancy category', 'galera'),
		);

		$taxonomy_rewrite = array(
			'slug'                  => 'vacancy-category',
			'with_front'            => true,
			'hierarchical'          => true,
		);

		$taxonomy_args = array(
			'labels'              => $taxonomy_labels,
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'show_in_nav_menus'   => true,
			'show_tagcloud'       => true,
			'rewrite'             => $taxonomy_rewrite,
		);
		register_taxonomy( 'vacancy-category', 'vacancy', $taxonomy_args );
    }

    public function enable_svg_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		$mimes['svgz'] = 'image/svg+xml';
		return $mimes;
	}

	public function wpdev_nav_classes( $classes, $item ) {
	    if( ( is_post_type_archive( 'case' ) || is_singular( 'case' ) )
	        && $item->title == 'Blog' ){
	        $classes = array_diff( $classes, array( 'current_page_parent' ) );
		} elseif( ( is_post_type_archive( 'service' ) || is_singular( 'service' ) )
	        && $item->title == 'Blog' ){
	        $classes = array_diff( $classes, array( 'current_page_parent' ) );
	    } elseif( ( is_post_type_archive( 'capability' ) || is_singular( 'capability' ) )
	        && $item->title == 'Blog' ){
	        $classes = array_diff( $classes, array( 'current_page_parent' ) );
	    } elseif( is_singular( 'case' ) && $item->object == 'case' ||
	    		  is_singular( 'service' ) && $item->object == 'service' || 
	    		  is_singular( 'capability' ) && $item->object == 'capability' ){
	    	$classes[] = 'current_page_parent';
	    }

	    return $classes;
	}

	public function js_variables(){ ?>
		<script type="text/javascript">
	        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	    </script>
	<?php }

	public function google_tag_manager() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/gtm' );
    	}
    }

    public function schema_org() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/schema' );
    	}
    }

    public function google_tag_manager_noscript() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/gtm', 'noscript' );
    	}
    }

    public function __return_false() {
        return false;
    }

	public function add_async_attribute($tag, $handle){
	    if(!is_admin()){
	        if ('jquery-core' == $handle) {
	            return $tag;
	        }
	        return str_replace(' src', ' defer src', $tag);
	    } else {
	        return $tag;
	    }
	}

    public function page_body_class($classes){
    	if( is_page() && !is_front_page() ){
    		$show = get_field('show_lines');
    		if(!$show) $classes[] = 'hide-lines';
    	} else if( is_home() || is_single() || is_404() || is_archive() ){
    		$classes[] = 'hide-lines';
    	}
    	return $classes;
    }
}

$theme_settings = new ThemeSettingsClass();