<?php
/**
 *
 * @package WordPress
 * @subpackage Galera
 * @since 1.0
 * @version 1.0
 */
get_header(); 

$galera = new GaleraClass();

get_template_part( 'template-parts/page/content-title', 'blog' ); ?>

<section class="posts__section">
	<div class="container">
		<div class="row">
			<?php echo $galera->get_posts(); ?>
		</div>
	</div>
</section>

<?php get_footer();