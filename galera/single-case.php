<?php
/**
 *
 * @package WordPress
 * @subpackage Galera
 * @since 1.0
 * @version 1.0
 */
get_header(); 

while ( have_posts() ) :

	the_post();

	get_template_part( 'template-parts/case/content', 'title' );

	if( have_rows('content') ):

		while ( have_rows('content') ) : the_row();

			if( get_row_layout() == 'text_section' ) :
				get_template_part( 'template-parts/case/content', 'text_section' );
			elseif( get_row_layout() == 'two_images_section' ) :
				get_template_part( 'template-parts/case/content', 'two_images_section' );
			elseif( get_row_layout() == 'before_after_section' ) :
				get_template_part( 'template-parts/case/content', 'before_after_section' );
			elseif( get_row_layout() == 'video_section' ) :
				get_template_part( 'template-parts/case/content', 'video_section' );
			elseif( get_row_layout() == 'video_slider' ) :
				get_template_part( 'template-parts/case/content', 'video_slider' );
			elseif( get_row_layout() == 'videos_grid' ) :
				get_template_part( 'template-parts/case/content', 'videos_grid' );
			elseif( get_row_layout() == 'capabilities_section' ) :
				get_template_part( 'template-parts/case/content', 'capabilities_section' );
			elseif( get_row_layout() == 'two_columns_section' ) :
				get_template_part( 'template-parts/case/content', 'two_columns_section' );
			elseif( get_row_layout() == 'two_columns_links' ) :
				get_template_part( 'template-parts/case/content', 'two_columns_links' );
			elseif( get_row_layout() == 'three_images_section' ) :
				get_template_part( 'template-parts/case/content', 'three_images_section' );
			elseif( get_row_layout() == 'awards_section' ) :
				get_template_part( 'template-parts/case/content', 'awards_section' );
			elseif( get_row_layout() == 'feedback_section' ) :
				get_template_part( 'template-parts/case/content', 'feedback_section' );
			endif;

		endwhile;

	endif;
	
endwhile; 

get_footer();