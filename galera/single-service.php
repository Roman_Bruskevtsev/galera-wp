<?php
/**
 *
 * @package WordPress
 * @subpackage Galera
 * @since 1.0
 * @version 1.0
 */

get_header(); 

if( have_rows('content') ):

	while ( have_rows('content') ) : the_row();

		if( get_row_layout() == 'full_width_video_section' ) :
			get_template_part( 'template-parts/capability/full_width_video_section' );
		elseif( get_row_layout() == 'quotes_section' ) :
			get_template_part( 'template-parts/capability/quotes_section' );
		elseif( get_row_layout() == 'use_cases_section' ) :
			get_template_part( 'template-parts/capability/use_cases_section' );
		elseif( get_row_layout() == 'gradient_contact_section' ) :
			get_template_part( 'template-parts/capability/gradient_contact_section' );
		elseif( get_row_layout() == 'how_it_can_help_section' ) :
			get_template_part( 'template-parts/capability/how_it_can_help_section' );
		elseif( get_row_layout() == 'companies_section' ) :
			get_template_part( 'template-parts/capability/companies_section' );
		elseif( get_row_layout() == 'faq_section' ) :
			get_template_part( 'template-parts/capability/faq_section' );
		elseif( get_row_layout() == 'banner_image_section' ) :
			get_template_part( 'template-parts/capability/banner_image_section' );
		elseif( get_row_layout() == 'awards_section' ) :
			get_template_part( 'template-parts/capability/awards_section' );
		elseif( get_row_layout() == 'team_section_grid' ) :
			get_template_part( 'template-parts/capability/team_section_grid' );
		elseif( get_row_layout() == 'latest_video_section' ) :
			get_template_part( 'template-parts/capability/latest_video_section' );
		elseif( get_row_layout() == 'feedback_slider' ) :
			get_template_part( 'template-parts/capability/feedback_slider_section' );
		elseif( get_row_layout() == 'ready_to_talk_section' ) :
			get_template_part( 'template-parts/capability/ready_to_talk_section' );
		elseif( get_row_layout() == 'latest_work_section' ) :
			get_template_part( 'template-parts/capability/latest_work_section' );
		elseif( get_row_layout() == 'banner_form_section' ) :
			get_template_part( 'template-parts/capability/banner_form_section' );
		elseif( get_row_layout() == 'grid_images' ) :
			get_template_part( 'template-parts/capability/grid_images' );
		elseif( get_row_layout() == 'two_images_section' ) :
			get_template_part( 'template-parts/capability/two_images_section' );
		endif;
	endwhile;

endif;

get_footer();