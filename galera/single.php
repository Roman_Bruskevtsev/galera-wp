<?php
/**
 *
 * @package WordPress
 * @subpackage Galera
 * @since 1.0
 * @version 1.0
 */
get_header(); 

while ( have_posts() ) :

	the_post();

	get_template_part( 'template-parts/post/content', 'title' );

	if( have_rows('content') ):

		while ( have_rows('content') ) : the_row();

			if( get_row_layout() == 'text_section' ) :
				get_template_part( 'template-parts/post/content', 'text_section' );
			elseif( get_row_layout() == 'two_images_section' ) :
				get_template_part( 'template-parts/post/content', 'two_images_section' );
			elseif( get_row_layout() == 'before_after_section' ) :
				get_template_part( 'template-parts/post/content', 'before_after_section' );
			elseif( get_row_layout() == 'video_section' ) :
				get_template_part( 'template-parts/post/content', 'video_section' );
			elseif( get_row_layout() == 'video_slider' ) :
				get_template_part( 'template-parts/post/content', 'video_slider' );
			elseif( get_row_layout() == 'videos_grid' ) :
				get_template_part( 'template-parts/case/content', 'videos_grid' );
			endif;

		endwhile;

	endif;

	get_template_part( 'template-parts/post/content', 'share' );
	
endwhile; 

get_footer();