<?php 
$thumbnail = get_the_post_thumbnail_url( get_the_ID() , 'post-thumbnail' ) ? 
			 ' style="background-image:url('.get_the_post_thumbnail_url( get_the_ID() , 'post-thumbnail' ).')"' : ''
?>
<div class="col-lg-6">
	<a href="<?php the_permalink(); ?>" class="service__block"<?php echo $thumbnail; ?> data-aos="fade-up" data-aos-duration="600">
		<div class="content">
			<div class="text">
				<h3><?php the_title(); ?></h3>
				<?php the_excerpt(); ?>
				<span class="text__link"><?php _e('Learn More', 'galera'); ?></span>
			</div>
		</div>
	</a>
</div>