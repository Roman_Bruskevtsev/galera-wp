<?php 
$background = get_sub_field('background_image') ? ' style="background-image: url('.get_sub_field('background_image').')"' : '';
$video = get_sub_field('video_id_vimeo');
?>
<section class="full__video__section"<?php echo $background; ?>>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-8">
				<div class="content" data-aos="fade-up" data-aos-duration="1500">
					<?php if( get_sub_field('title') ) { ?><h5 class="text-center"><b><?php the_sub_field('title'); ?></b></h5><?php } 
					the_sub_field('text'); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if( $video ) { ?><div class="play show__video" data-src="https://player.vimeo.com/video/<?php echo $video; ?>"></div><?php } ?>
</section>