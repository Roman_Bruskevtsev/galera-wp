<section class="how__it__section">
	<div class="container">
	<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="title" data-aos="fade-right" data-aos-duration="1500"><h2><?php the_sub_field('title'); ?></h2></div>
			</div>
		</div>
	<?php } 
	$blocks = get_sub_field('blocks'); 
	if( $blocks ) { ?>
		<div class="row justify-content-center">
		<?php
		$i = 1; 
		foreach ( $blocks as $block ) { ?>
			<div class="col-lg-10">
				<div class="block">
					<div class="number" data-aos="fade-right" data-aos-duration="1000">
						<h2 class="gradient vertical"><?php echo $i; ?></h2>
					</div>
					<div class="group__blocks">
						<div class="left" data-aos="fade-left" data-aos-duration="1000">
							<h5><b><?php echo $block['left_side']['title']; ?></b></h5>
							<p><?php echo $block['left_side']['text']; ?></p>
						</div>
						<div class="right" data-aos="fade-up" data-aos-duration="1500">
							<h4><b><?php echo $block['right_side']['title']; ?></b></h4>
							<p><?php echo $block['right_side']['text']; ?></p>
						</div>
					</div>
				</div>
			</div>
		<?php $i++; } ?>
		</div>
	<?php } ?>
	</div>
</section>