<section class="latest__video__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="title" data-aos="fade-right" data-aos-duration="1500">
					<h3><?php the_sub_field('title'); ?></h3>
				</div>
			</div>
		</div>
		<?php } 
		$slider = get_sub_field('videos'); 
		if( $slider ) { ?>
		<div class="row">
			<div class="col-lg-10">
				<div class="latest__slider" data-aos="fade-up" data-aos-duration="600">
				<?php foreach ( $slider as $slide ) { 
					$image = $slide['preview_image'] ? ' style="background-image: url('.$slide['preview_image']['url'].')"' : ''; ?>
					<div class="slide"<?php echo $image; ?>>
						<div class="content">
							<?php if( $slide['name'] ) { ?><h5><b><?php echo $slide['name']; ?></b></h5><?php } ?>
							<?php if( $slide['title'] ) { ?><h3><?php echo $slide['title']; ?></h3><?php } ?>
							<?php if( $slide['video_id_vimeo'] ) { ?>
								<div class="play show__video" data-src="https://player.vimeo.com/video/<?php echo $slide['video_id_vimeo']; ?>"></div>
							<?php } ?>
						</div>
					</div>
				<?php } ?>
				</div>
				<div class="slider__nav"></div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>