<?php if( get_sub_field('text') ) { ?>
<section class="quotes__section">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-3"></div>
			<div class="col-lg-7">
				<blockquote>
					<span class="icon"></span>
					<div class="text" data-aos="fade-left" data-aos-duration="1500"><?php the_sub_field('text'); ?></div>
				</blockquote>
			</div>
		</div>
	</div>
</section>
<?php } ?>