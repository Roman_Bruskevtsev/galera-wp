<section class="ready__talk__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="content">
					<div class="ready__text">
						<div class="row">
							<div class="col-lg-6 order-last">
								<div class="person">
									<?php if( get_sub_field('person_avatar') ) { ?>
									<img src="<?php echo get_sub_field('person_avatar')['url']; ?>" alt="<?php echo get_sub_field('person_avatar')['title']; ?>">
									<?php } ?>
									<div class="person__info">
										<?php if( get_sub_field('name') ) { ?><h4><?php the_sub_field('name'); ?></h4><?php } ?>
										<?php if( get_sub_field('position') ) { ?><p><?php the_sub_field('position'); ?></p><?php } ?>
									</div>
								</div>
							</div>
							<div class="col-lg-6 order-first">
								<div class="text">
									<?php if( get_sub_field('title') ) { ?><h2><?php the_sub_field('title'); ?></h2><?php } ?>
									<?php if( get_sub_field('button_link') ) { ?><a class="btn btn__gradient" href="<?php the_sub_field('button_link'); ?>"><span><?php the_sub_field('button_text'); ?></span></a><?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>