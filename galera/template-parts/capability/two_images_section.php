<section class="two__images__section">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="title text-center" data-aos="fade-up" data-aos-duration="600">
					<?php if( get_sub_field('title') ) { ?><h3><?php the_sub_field('title'); ?></h3><?php } ?>
					<?php if( get_sub_field('subtitle') ) { ?><p><?php the_sub_field('subtitle'); ?></p><?php } ?>
				</div>
			</div>
		</div>
		<div class="row">
			<?php if( get_sub_field('image_1') ) { ?>
			<div class="col-lg-6">
				<div class="image" data-aos="fade-right" data-aos-duration="600">
					<img src="<?php echo get_sub_field('image_1')['url']; ?>" alt="<?php echo get_sub_field('image_1')['title']; ?>">
				</div>
			</div>
			<?php } ?>
			<?php if( get_sub_field('image_2') ) { ?>
			<div class="col-lg-6">
				<div class="image" data-aos="fade-left" data-aos-duration="600">
					<img src="<?php echo get_sub_field('image_2')['url']; ?>" alt="<?php echo get_sub_field('image_2')['title']; ?>">
				</div>
			</div>
			<?php } ?>
		</div>	
	</div>
</section>