<section class="awards__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<div class="awards__title">
					<?php if( get_sub_field('gradient_title') ) { ?><h2 class="gradient" data-aos="fade-right" data-aos-duration="2000"><?php the_sub_field('gradient_title'); ?></h2><?php } ?>
					<?php if( get_sub_field('title') ) { ?><h3 data-aos="fade-right" data-aos-duration="1000"><?php the_sub_field('title'); ?></h3><?php } ?>
				</div>
			</div>
			<div class="col-lg-5">
				<?php 
				$awards = get_sub_field('awards');
				
				if( $awards ) { ?>
					<div class="awards__block" data-aos="fade-up" data-aos-duration="1000">
						<div class="ribbons">
							<ul>
							<?php 
							$i = 0;
							foreach ($awards as $award) { 
								$active = $i == 0 ? ' active' : ''; ?>
								<li class="cursor__hover<?php echo $active; ?>" onclick="galera.showAward(this)"><span><?php echo $award['title']; ?></span></li>
							<?php $i++; } ?>
							</ul>
						</div>
						<div class="texts">
							<?php
							$i = 0;
							foreach ($awards as $award) { 
								$active = $i == 0 ? ' active' : ''; ?>
								<div class="description<?php echo $active; ?>"><?php echo $award['text']; ?></div>
							<?php $i++; } ?>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>