<section class="capabilities__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="title" data-aos="fade-left" data-aos-duration="1500">
					<h4><?php the_sub_field('title'); ?></h4>
				</div>
			</div>
		</div>
		<?php } 
		$capabilities = get_sub_field('capabilities');
		if( $capabilities ){ ?>
		<div class="row">
			<div class="col">
				<div class="capabilities">
				<?php foreach ($capabilities as $capability) { ?>
					<div class="capability">
						<?php if( $capability['icon'] ) { ?>
						<div class="icon">
							<img src="<?php echo $capability['icon']['url']; ?>" width="<?php echo $capability['icon']['width']; ?>" height="<?php echo $capability['icon']['height']; ?>" alt="<?php echo $capability['icon']['title']; ?>">
						</div>
						<?php } ?>
						<h6><?php echo $capability['title']; ?></h6>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>