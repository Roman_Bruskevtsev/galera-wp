<section class="testimonial">
	<div class="container">
		<div class="row">
			<div class="col">
			<?php if( get_sub_field('text') ) { ?>
				<div class="text">
					<?php $logo = get_sub_field('logo'); ?>
					<div class="logo">
						<?php if( $logo ) { ?>
							<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['name']; ?>">
						<?php } ?>
					</div>
					<p><?php the_sub_field('text'); ?></p>
					<?php if( get_sub_field('avatar') ) { 
						$avatar = get_sub_field('avatar'); ?>
						<div class="avatar">
							<img src="<?php echo $avatar; ?>" alt="<?php echo get_sub_field('name'); ?>">
						</div>
					<?php } ?>
				</div>
			<?php } 
			if( get_sub_field('name') || get_sub_field('position') ) { ?>
				<div class="author">
					<h5><b><?php echo get_sub_field('name'); ?></b></h5>
					<span class="position"><?php echo get_sub_field('position'); ?></span>
				</div>
			<?php } ?>
			</div>
		</div>
	</div>
</section>