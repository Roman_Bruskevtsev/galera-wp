<section class="sharing__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="share__block text-center">
					<h3><?php _e('Share:', 'galera'); ?></h3>
					<ul>
						<li>
							<button data-network="linkedin" data-url="<?php the_permalink(); ?>" data-title="<?php the_title(); ?>" data-image="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full' ); ?>" data-description="<?php echo get_the_excerpt(); ?>" class="share linkedin st-custom-button"></button>
						</li>
						<li>
							<button data-network="facebook" data-url="<?php the_permalink(); ?>" data-title="<?php the_title(); ?>" data-image="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full' ); ?>" data-description="<?php echo get_the_excerpt(); ?>" class="share facebook st-custom-button"></button>
						</li>
						<li>
							<button data-network="email" data-url="<?php the_permalink(); ?>" data-title="<?php the_title(); ?>" data-image="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full' ); ?>" data-description="<?php echo get_the_excerpt(); ?>" class="share email st-custom-button"></button>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>