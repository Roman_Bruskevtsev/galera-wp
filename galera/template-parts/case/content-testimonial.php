<?php 
$testimonial = get_field('feedback'); 
if($testimonial) { ?>
<section class="testimonial">
	<div class="container">
		<div class="row">
			<div class="col">
			<?php if( $testimonial['text'] ) { ?>
				<div class="text">
					<?php $logo = $testimonial['logo']; ?>
					<div class="logo">
						<?php if( $logo ) { ?>
							<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['name']; ?>">
						<?php } ?>
					</div>
					<p><?php echo $testimonial['text']; ?></p>
					<?php if( $testimonial['avatar'] ) { 
						$avatar = $testimonial['avatar']; ?>
						<div class="avatar">
							<img src="<?php echo $avatar; ?>" alt="<?php echo $testimonial['name']; ?>">
						</div>
					<?php } ?>
				</div>
			<?php } 
			if( $testimonial['name'] || $testimonial['position'] ) { ?>
				<div class="author">
					<h5><b><?php echo $testimonial['name']; ?></b></h5>
					<span class="position"><?php echo $testimonial['position']; ?></span>
				</div>
			<?php } ?>
			</div>
		</div>
	</div>
</section>
<?php } ?>