<?php if( get_sub_field('text') ) { ?>
<section class="text__section">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="content"><?php the_sub_field('text'); ?></div>
			</div>
		</div>
	</div>
</section>
<?php } ?>