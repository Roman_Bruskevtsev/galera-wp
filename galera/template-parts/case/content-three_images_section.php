<section class="three__images__section">
	<div class="container">
		<?php 
		$image_1 = get_sub_field('image_1'); 
		$image_2 = get_sub_field('image_2'); 
		$image_3 = get_sub_field('image_3'); 
		if( $image_1 ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<img src="<?php echo $image_1['url']; ?>" width="<?php echo $image_1['width']; ?>" height="<?php echo $image_1['height']; ?>" alt="<?php echo $image_1['title']; ?>">
			</div>
		</div>
		<?php }
		if( $image_2 || $image_3 ) { ?>
			<div class="row">
				<?php if( $image_2 ) { ?>
				<div class="col-lg-6">
					<img src="<?php echo $image_2['url']; ?>" width="<?php echo $image_2['width']; ?>" height="<?php echo $image_2['height']; ?>" alt="<?php echo $image_2['title']; ?>">
				</div>
				<?php } ?>
				<?php if( $image_3 ) { ?>
				<div class="col-lg-6">
					<img src="<?php echo $image_3['url']; ?>" width="<?php echo $image_3['width']; ?>" height="<?php echo $image_3['height']; ?>" alt="<?php echo $image_3['title']; ?>">
				</div>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
</section>