<section class="post__title">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="post__nav" data-aos="fade-left">
					<a href="<?php echo get_post_type_archive_link( 'case' ); ?>"><?php _e('Back', 'galera'); ?></a>
				</div>
				<div class="post__banner" data-aos="fade-up" data-aos-duration="600">
					<h5><span><?php _e('Case study', 'galera'); ?></span></h5>
					<h1><?php the_title(); ?></h1>
				</div>
				<?php 
				$video = get_field('case_video');
				if( $video['video_id_vimeo'] ) { 
					$background = $video['preview'] ? ' style="background-image: url('.$video['preview']['url'].')"' : ''; ?>
				<div class="post__video" data-aos="fade-up" data-aos-duration="600">
					<div class="preview"<?php echo $background; ?>></div>
					<div class="play"></div>
					<iframe data-src="https://player.vimeo.com/video/<?php echo $video['video_id_vimeo']; ?>?title=0&byline=0" allow="autoplay; fullscreen"></iframe>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>