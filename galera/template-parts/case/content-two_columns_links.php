<section class="two__columns__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-5">
			<?php if( get_sub_field('left_side_text') ) { ?>
				<div class="text"><?php the_sub_field('left_side_text'); ?></div>
			<?php } ?>
			</div>
			<div class="col-lg-1"></div>
			<div class="col-lg-5">
				<?php 
				$right_block = get_sub_field('right_side_block');
				if( $right_block ) { 
					$links = $right_block['links'];
					?>
				<div class="text">
					<?php if( $right_block['title'] ) { ?><h4><b><?php echo $right_block['title']; ?></b></h4><?php } ?>
					<?php if( $links ) { ?>
					<ul class="links">
					<?php foreach ($links as $link) { ?>
						<li>
							<a class="link" href="<?php echo $link['link']; ?>" target="_blank"><?php echo $link['title']; ?></a>
						</li>
					<?php } ?>
					</ul>
					<?php } ?>
				</div>
			<?php } ?>
			</div>
			<div class="col-lg-1"></div>
		</div>
	</div>
</section>