<section class="two__columns__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-5">
			<?php if( get_sub_field('left_side_text') ) { ?>
				<div class="text"><?php the_sub_field('left_side_text'); ?></div>
			<?php } ?>
			</div>
			<div class="col-lg-1"></div>
			<div class="col-lg-5">
				<?php if( get_sub_field('right_side_text') ) { ?>
				<div class="text"><?php the_sub_field('right_side_text'); ?></div>
			<?php } ?>
			</div>
			<div class="col-lg-1"></div>
		</div>
	</div>
</section>