<?php 
$videos = get_sub_field('videos'); ?>
<section class="videos__grid">
	<div class="container">
		<?php 
		if( get_sub_field('text') ) { ?>
		<div class="row">
			<div class="col">
				<div class="text"><?php the_sub_field('text'); ?></div>
			</div>
		</div>
		<?php }
		if( $videos ) { ?>
		<div class="row">
			<?php
			foreach ( $videos as $video ) {
			$preview = $video['preview_image'] ? ' style="background-image:url('.$video['preview_image']['url'].')"' : ''; ?>
			<div class="col-lg-6">
				<div class="video__cell">
					<div class="preview"<?php echo $preview; ?>></div>
					<?php $video = $video['video_id_vimeo']; 
					if( $video ) { ?>
					<button class="play__btn" onclick="galera.playCellVideo('<?php echo 'https://player.vimeo.com/video/'.$video; ?>', this);"></button>
					<?php } ?>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="row">
			<div class="col">
				<div class="video__popup">
					<div class="wrapper"><span class="close" onclick="galera.closeCellVideo(this);"></span></div>
					<div class="popup">
						<div class="video"></div>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>