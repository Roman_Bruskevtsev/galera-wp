<?php 
$show_form = get_field('show_form_section', 'option');
if( $show_form ) { ?>
<section class="form__section">
	<div class="container">
		<?php if( get_field('form_title', 'option') ) { ?>
		<div class="row">
			<div class="col-lg-6">
				<div class="section__title">
					<h2><?php the_field('form_title', 'option'); ?></h2>
					<?php if( get_field('button_text', 'option') && get_field('contact_us_page', 'option') ) { ?>
						<a href="<?php echo get_permalink( get_field('contact_us_page', 'option') ); ?>" class="btn btn__gradient"><span><?php the_field('button_text', 'option'); ?></span></a>
					<?php } ?>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="circle"></div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>
<?php } 