<?php 
$newsletter = get_field('newsletter', 'option');
?>
<section class="newsletter__section">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10">
				<div class="form__section text-center">
					<?php if( $newsletter['title'] ) { ?><h2><?php echo $newsletter['title']; ?></h2><?php } ?>
					<?php if( $newsletter['text'] ) { ?><p><?php echo $newsletter['text']; ?></p><?php } ?>
					<?php if( $newsletter['form_shortcode'] ) echo do_shortcode( $newsletter['form_shortcode'] ); ?>
				</div>
			</div>
		</div>
	</div>
</section>