<?php 
$video = get_sub_field('video');
?>
<section class="banner__text__section">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="banner__block">
					<?php if( $video['url'] ) { ?>
					<div class="video">
						<video autoplay muted loop>
							<source src="<?php echo $video['url']; ?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
						</video>
					</div>
					<?php } ?>
					<div class="content" data-aos="fade-up" data-aos-duration="1500">
						<?php if( get_sub_field('text') ) { ?><h3><?php the_sub_field('text'); ?></h3><?php } ?>
						<?php if( get_sub_field('author') ) { ?><p><?php the_sub_field('author'); ?></p><?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>