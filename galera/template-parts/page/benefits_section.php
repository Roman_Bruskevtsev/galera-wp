<section class="benefits__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="section__title" data-aos="fade-right" data-aos-duration="1500">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
	</div>
	<?php $benefits = get_sub_field('benefits'); 
	if( $benefits ) { ?>
	<!-- <div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 nopadding">
				<div class="border"></div>
			</div>
		</div>
	</div> -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 nopadding"><div class="border"></div></div>
			<div class="col-lg-6">
			<?php foreach ( $benefits as $benefit ) { ?>
				<div class="row border__row">
					<div class="col-lg-8 nopadding">
						<div class="benefit__block" data-aos="fade-left" data-aos-duration="1500">
							<h3><?php echo $benefit['title']; ?></h3>
							<?php echo $benefit['text']; ?>
						</div>
					</div>
				</div>
			<?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>
</section>