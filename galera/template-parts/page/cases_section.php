<section class="cases__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-6">
				<div class="section__title" data-aos="fade-right" data-aos-duration="1500">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
		<?php } 
		$cases = get_sub_field('choose_cases'); 
		$args = array(
			'posts_per_page' 	=> -1,
			'orderby' 			=> 'comment_count',
			'post_type'			=> 'case',
			'orderby'			=> 'post__in',
			'post__in'			=> $cases
		);
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) { ?>
		<div class="row">
			<div class="col">
				<div class="cases__slider" data-aos="fade-up" data-aos-duration="500">
				<?php while ( $query->have_posts() ) { $query->the_post();
					$thumbnail = get_the_post_thumbnail( get_the_ID() ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'cases-image' ).')"': ''; 
					$feedback = get_field('feedback'); 
					$title = get_field('short_title') ? get_field('short_title') : get_the_title(); ?>
					<div class="slide">
						<div class="top__block">
							<div class="image"><div class="thumbnail"<?php echo $thumbnail; ?>></div></div>
							<div class="title">
								<div class="content">
									<h3><?php echo $title; ?></h3>
									<a href="<?php the_permalink(); ?>" class="btn btn__gradient"><span><?php _e('Read more', 'galera'); ?></span></a>
								</div>
							</div>
							
						</div>
						<?php if( $feedback ) { 
							$avatar = $feedback['avatar'] ? ' style="background-image: url('.$feedback['avatar'].');"' : ''; ?>
						<div class="bottom__block">
							<div class="author text-center">
								<?php if( $feedback['avatar'] ) { ?><div class="avatar"<?php echo $avatar; ?>></div><?php } ?>
								<?php if( $feedback['name'] ) { ?><h4><?php echo $feedback['name']; ?></h4><?php } ?>
								<?php if( $feedback['position'] ) { ?><p><?php echo $feedback['position']; ?></p><?php } ?>
							</div>
							<?php if( $feedback['text'] ) { ?>
								<div class="text"><p><?php echo $feedback['text']; ?></p></div>
							<?php } ?>
							<a href="<?php the_permalink(); ?>" class="d-block d-lg-none"><span><?php _e('Read more', 'galera'); ?></span></a>
						</div>
						<?php } ?>
					</div>
				<?php } ?>
				</div>
				<!-- <div class="cases__navigation"></div> -->
			</div>
		</div>
		<?php } wp_reset_postdata(); 
		$link = get_sub_field('link'); 
		if( $link ) { 
			$target = $link['target'] ? ' target="'.$link['target'].'"' : ''; ?>
		<div class="row">
			<div class="col">
				<div class="button__row text-center">
					<a class="btn btn__red" href="<?php echo $link['url']; ?>"<?php echo $target; ?>><span><?php echo $link['title']; ?></span></a>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>