<section class="character__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<?php if( get_sub_field('text') ) { ?><div class="text" data-aos="fade-right" data-aos-duration="1500"><?php the_sub_field('text'); ?></div><?php } ?>
			</div>
			<div class="col-lg-5">
				<?php if( get_sub_field('image') ) { ?>
				<div class="image" data-aos="fade-up" data-aos-duration="1500">
					<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>">
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>