<section class="contact__form__section">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="form__block">
					<?php if( get_sub_field('title') ) { ?>
					<div class="row">
						<div class="col">
							<h2 class="h1 text-center" data-aos="fade-up" data-aos-duration="1500"><?php the_sub_field('title'); ?></h2>
						</div>
					</div>
					<?php } 
					if( get_sub_field('email') || get_sub_field('phone') || get_sub_field('address') ) { ?>
					<div class="row">
						<div class="col">
						<?php if( get_sub_field('email') ) { ?>
							<div class="contact__block email" data-aos="fade-up" data-aos-duration="1500">
								<h4><b><?php _e('Email', 'galera'); ?></b></h4>
								<a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a>
							</div>
						<?php } 
						if( get_sub_field('phone') ) { ?>
							<div class="contact__block phone" data-aos="fade-up" data-aos-duration="1500">
								<h4><b><?php _e('Phone', 'galera'); ?></b></h4>
								<a href="tel:<?php the_sub_field('phone'); ?>"><?php the_sub_field('phone'); ?></a>
							</div>
						<?php } 
						if( get_sub_field('address') ) { ?>
							<div class="contact__block address" data-aos="fade-up" data-aos-duration="1500">
								<h4><b><?php _e('Address', 'galera'); ?></b></h4>
								<a href="https://www.google.com/maps/place/<?php the_sub_field('address'); ?>" target="_blank"><?php the_sub_field('address'); ?></a>
							</div>
						<?php } ?>
						</div>
					</div>
					<?php } ?>
					<div class="row">
						<div class="col">
							<div class="form__wrapper" data-aos="fade-up" data-aos-duration="1500">
								<form class="validation__form"  autocomplete="off" action="<?php echo get_the_permalink(); ?>"  onsubmit="return formClass.validation(this);" name="contact-form" method="post" accept-charset="utf-8" novalidate>
									<?php wp_nonce_field('security', 'form-security_'.wp_unique_id()); ?>
									<input type="text" class="form__field" name="subject" value="<?php _e('Contact form', 'galera'); ?>" hidden readonly required>
									<div class="field__group">
										<label>
											<input type="text" name="question" class="form__field" placeholder="" required onkeyup="formClass.checkField(this);">
											<span class="placeholder"><?php _e('How can we help you?', 'bca'); ?></span>
										</label>
									</div>
									<div class="field__group">
										<label>
											<input type="email" name="email" class="form__field" placeholder="" required onkeyup="formClass.checkField(this);">
											<span class="placeholder"><?php _e('E-mail:', 'bca'); ?></span>
										</label>
									</div>
									<div class="field__group">
										<label>
											<input type="text" name="name" class="form__field" placeholder="" required onkeyup="formClass.checkField(this);">
											<span class="placeholder"><?php _e('Name:', 'bca'); ?></span>
										</label>
									</div>
									<input class="form__field" name="bot-checker" hidden>
									<!-- <div class="field__group">
										<label>
											<input type="text" name="company" class="form__field" placeholder="" required onkeyup="formClass.checkField(this);">
											<span class="placeholder"><?php _e('Company:', 'bca'); ?></span>
										</label>
									</div> -->
									
									<!-- <div class="field__group">
										<label>
											<select name="budget" class="form__field" onchange="formClass.checkField(this);">
												<option value=""></option>
												<option value="less than $5,000">Less than $5,000</option>
												<option value="$5,000-$10,000">$5,000-$10,000</option>
												<option value="$10,000+">$10,000+</option>
											</select>
											<span class="placeholder"><?php _e('What’s the budget?', 'bca'); ?></span>
										</label>
									</div> -->
									<div class="submit__group">
										<button class="btn btn__full__red large full" type="submit"><span><?php _e('Send', 'galera'); ?></span></button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>