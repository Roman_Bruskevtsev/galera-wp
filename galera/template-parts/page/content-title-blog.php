<?php 
$page_id = get_option('page_for_posts'); 
$subtitle = get_field('subtitle', $page_id);
?>
<section class="page__title">
	<div class="container">
		<div class="row">
			<div class="col">
				<h1 data-aos="fade-right" data-aos-duration="1500"><?php echo get_the_title($page_id); ?></h1>
			</div>
		</div>
		<?php if( $subtitle ) { ?>
		<div class="row">
			<div class="col">
				<div class="text" data-aos="fade-up" data-aos-duration="1500"><p><?php echo $subtitle; ?></p></div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>