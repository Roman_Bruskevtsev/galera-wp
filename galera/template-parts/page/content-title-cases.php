<?php 
$cases = get_field('cases', 'option');
?>
<section class="page__title">
	<div class="container">
		<?php if( $cases['title'] ) { ?>
		<div class="row">
			<div class="col">
				<h1 data-aos="fade-right" data-aos-duration="1500"><?php echo $cases['title']; ?></h1>
			</div>
		</div>
		<?php } 
		if( $cases['subtitle']  ) { ?>
		<div class="row">
			<div class="col">
				<div class="text" data-aos="fade-up" data-aos-duration="1500"><p><?php echo $cases['subtitle']; ?></p></div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>