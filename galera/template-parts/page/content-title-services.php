<?php 
$services = get_field('services', 'option');
?>
<section class="page__title">
	<div class="container">
		<?php if( $services['title'] ) { ?>
		<div class="row">
			<div class="col">
				<h1 data-aos="fade-right" data-aos-duration="1500"><?php echo $services['title']; ?></h1>
			</div>
		</div>
		<?php } 
		if( $services['subtitle']  ) { ?>
		<div class="row">
			<div class="col">
				<div class="text" data-aos="fade-up" data-aos-duration="1500"><p><?php echo $services['subtitle']; ?></p></div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>