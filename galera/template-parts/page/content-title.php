<?php 
$show_title = get_field('show_title');
$show_quote = get_field('show_quote');
?>
<?php if( $show_title ) { ?>
<section class="page__title">
	<div class="container">
		<div class="row">
			<div class="col">
				<h1 data-aos="fade-right" data-aos-duration="1500"><?php the_title(); ?></h1>
			</div>
		</div>
		<?php }
		if( $show_quote ) { 
			$quote = get_field('quote');
			if( $quote ) { ?>
				<div class="row justify-content-end">
					<div class="col-md-6">
						<div class="quote__block text-right" data-aos="fade-left" data-aos-duration="1500">
							<?php if( $quote['text'] ) { ?><h4><?php echo $quote['text']; ?></h4><?php } ?>
							<?php if( $quote['author'] ) { ?><p><?php echo $quote['author']; ?></p><?php } ?>
						</div>
					</div>
				</div>	
			<?php }
		} ?>
	</div>
</section>