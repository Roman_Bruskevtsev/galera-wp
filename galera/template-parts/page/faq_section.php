<section class="faq__section">
	<div class="container">
	<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="title" data-aos="fade-right" data-aos-duration="1500"><h3><?php the_sub_field('title'); ?></h3></div>
			</div>
		</div>
	<?php } 
	$questions = get_sub_field('questions'); 
	if( $questions ) { ?>
		<div class="row">
			<?php foreach ( $questions as $question ) { ?>
			<div class="col-lg-6">
				<div class="question" data-aos="fade-up" data-aos-duration="1500">
					<?php if( $question['title'] ) { ?><h4><b><?php echo $question['title']; ?></b></h4><?php } ?>
					<?php if( $question['text'] ) { ?><p><?php echo $question['text']; ?></p><?php } ?>
				</div>
			</div>
			<?php } ?>
		</div>
	<?php } ?>
	</div>
</section>