<section class="feedback__slider__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php 
				$slider = get_sub_field('slider'); 
				if( $slider ) { ?>
				<div class="feedback__slider">
				<?php foreach ( $slider as $slide ) { ?>
					<div class="slide">
					<?php if( $slide['text'] ) { ?>
						<div class="text">
							<?php $logo = $slide['logo']; ?>
							<div class="logo">
								<?php if( $logo ) { ?>
									<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['name']; ?>">
								<?php } ?>
							</div>
							<p><?php echo $slide['text']; ?></p>
							<?php if( $slide['avatar'] ) { 
								$avatar = $slide['avatar']; ?>
								<div class="avatar">
									<img src="<?php echo $avatar; ?>" alt="<?php echo $slide['name']; ?>">
								</div>
							<?php } ?>
						</div>
					<?php } 
					if( $slide['name'] || $slide['position'] ) { ?>
						<div class="author">
							<h5><b><?php echo $slide['name']; ?></b></h5>
							<span class="position"><?php echo $slide['position']; ?></span>
						</div>
					<?php } ?>
					</div>
				<?php } ?>
				</div>
				<div class="slider__nav"></div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>