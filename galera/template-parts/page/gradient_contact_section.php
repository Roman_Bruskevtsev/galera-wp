<section class="gradient__contact__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="content text-center">
					<div class="row justify-content-center">
						<div class="col-lg-8">
							<div class="text" data-aos="fade-up" data-aos-duration="1500">
								<?php if( get_sub_field('title') ) { ?><h2><?php the_sub_field('title'); ?></h2><?php } ?>
								<p><?php the_sub_field('text'); ?></p>
								<?php if( get_sub_field('button_text') ) { ?><a class="btn btn__gradient" href="<?php the_sub_field('button_url'); ?>"><span><?php the_sub_field('button_text'); ?></span></a><?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>