<section class="image__slider__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="title" data-aos="fade-right" data-aos-duration="1500">
					<h3><?php the_sub_field('title'); ?></h3>
				</div>
			</div>
		</div>
		<?php }
		$images = get_sub_field('images'); 
		if( $images ) { ?>
		<div class="row">
			<div class="col-lg-10">
				<div class="images__slider" data-aos="fade-up" data-aos-duration="600">
				<?php foreach ( $images as $image ) { ?>
					<div class="slide">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
					</div>
				<?php } ?>
				</div>
				<div class="slider__nav"></div>
			</div>
		</div>
		<?php } 
		$button_link = get_sub_field('button_link'); 
		if( $button_link ) { ?>
		<div class="row">
			<div class="col">
				<div class="button__row text-center">
					<a href="<?php the_sub_field('button_link'); ?>" class="btn btn__white"><span><?php the_sub_field('button_label'); ?></span></a>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>