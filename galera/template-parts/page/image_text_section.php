<section class="image__text__section">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<?php 
				$image = get_sub_field('image');
				if( $image ) { ?>
				<div class="image">
					<div class="image__block image__slide">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="col-md-6" data-aos="fade-left" data-aos-duration="1000">
				<?php if( get_sub_field('text') ) { ?>
				<div class="text">
					<?php the_sub_field('text'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>