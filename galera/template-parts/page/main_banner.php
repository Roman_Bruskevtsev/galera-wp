<section class="main__banner">
	<div class="bubbles__wrapper">
		<div class="left__bubbles">
			<div class="large bubble"></div>
			<div class="middle bubble layer" data-depth="0.1"></div>
			<div class="small bubble layer" data-depth="0.3"></div>
		</div>
		<div class="right__bubbles">
			<div class="large bubble"></div>
			<div class="middle bubble"></div>
			<div class="small bubble layer" data-depth="0.2"></div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
			<?php if( get_sub_field('title') ) { ?>
				<div class="title" data-aos="fade-right" data-aos-duration="2000">
					<h1 class="h2"><?php the_sub_field('title'); ?></h1>
				</div>
			<?php } 
			if( get_sub_field('subtitle') ) { ?>
				<div class="subtitle" data-aos="fade-right" data-aos-duration="2000"><?php the_sub_field('subtitle'); ?></div>
			<?php } ?>
			</div>
			<div class="col-lg-5">
			<?php if( get_sub_field('text') ) { ?>
				<div class="text text-left" data-aos="fade-left" data-aos-duration="2000">
					<?php the_sub_field('text'); ?>
					<?php if( get_field('contact_us_page', 'option') ) { ?>
						<div class="d-block d-lg-none"><a href="<?php echo get_permalink( get_field('contact_us_page', 'option') ); ?>" class="btn btn__red"><span><?php _e('Contact us', 'galera'); ?></span></a></div>
                    <?php } ?>
				</div>
			<?php } ?>
			</div>
		</div>
		<?php 
		$video = get_sub_field('video_id'); 
		if( $video ) { 
			$background = get_sub_field('video_preview_image') ? ' style="background-image: url('.get_sub_field('video_preview_image').')"' : '';
		?>
		<div class="row justify-content-md-center">
			<div class="col-lg-10">
				<div class="main__video" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">
					<div class="preview"<?php echo $background; ?>></div>
					<div class="play"></div>
					<iframe data-src="https://player.vimeo.com/video/<?php echo $video; ?>?title=0&byline=0&controls=0" frameborder="0" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
				<?php 
				$awards_block = get_sub_field('awards_block');
				
				if( $awards_block ) { 
					$awards = $awards_block['awards']; ?>
					<div class="awards__block" data-aos="fade-up" data-aos-duration="1000">
						<?php if( $awards_block['title'] ) { ?><h3><?php echo $awards_block['title']; ?></h3><?php } ?>
						<?php if( $awards ) { ?>
							<div class="ribbons">
								<ul>
								<?php 
								$i = 0;
								foreach ($awards as $award) { 
									$active = $i == 0 ? ' active' : ''; ?>
									<li class="cursor__hover<?php echo $active; ?>" onclick="galera.showAward(this)"><span><?php echo $award['title']; ?></span></li>
								<?php $i++; } ?>
								</ul>
							</div>
							<div class="texts">
								<?php
								$i = 0;
								foreach ($awards as $award) { 
									$active = $i == 0 ? ' active' : ''; ?>
									<div class="description<?php echo $active; ?>"><?php echo $award['text']; ?></div>
								<?php $i++; } ?>
							</div>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
		<?php } ?>
	</div>
</section>