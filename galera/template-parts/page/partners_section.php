<section class="partners__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-6">
				<div class="section__title" data-aos="fade-right" data-aos-duration="1500">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
		<?php } 
		$partners = get_sub_field('partners'); 
		if( $partners ) { ?>
		<div class="row">
			<div class="col">
				<div class="partners">
				<?php foreach ($partners as $partner) { ?>
					<div class="slide" data-aos="fade-up" data-aos-duration="500">
						<div class="logo">
							<?php if( $partner['link'] ) { ?><a href="<?php echo $partner['link']; ?>" target="_blank"><?php } ?>
							<?php if( $partner['logo'] ) { ?><img src="<?php echo $partner['logo']['url']; ?>" alt="<?php echo $partner['logo']['title']; ?>"><?php } ?>
							<?php if( $partner['link'] ) { ?></a><?php } ?>
						</div>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>