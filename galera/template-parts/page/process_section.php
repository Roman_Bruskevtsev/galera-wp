<section class="process__section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 nopadding">
				<div class="border"></div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="section__title" data-aos="fade-right" data-aos-duration="1500">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
		<?php $process = get_sub_field('process'); 
		$array_size = count($process);
		if( $process ) { ?>
		<div class="row">
			<div class="col-lg-12">
			<?php $i = 0;
			foreach ($process as $proces) {
				if( $i % 2 == 0 && $i != $array_size - 1 ) {	?>
				<div class="process__row text__image">
					<div class="row">
						<div class="col-lg-5"></div>
						<div class="col-lg-7">
							<div class="image paroller">
								<img src="<?php echo $proces['image']['url']; ?>" title="<?php echo $proces['image']['title']; ?>" data-paroller-factor="0.05" data-aos="fade-up" data-aos-duration="1500">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 nopadding">
							<div class="text" data-aos="fade-right" data-aos-duration="1500">
								<h3><?php echo $proces['title']; ?></h3>
								<?php echo $proces['text']; ?>
							</div>
						</div>
					</div>
				</div>
				<?php } elseif ( $i % 2 != 0 && $i != $array_size - 1 ) { ?>
				<div class="process__row image__text">
					<div class="row">
						<div class="col-lg-6">
							<div class="image paroller">
								<img src="<?php echo $proces['image']['url']; ?>" title="<?php echo $proces['image']['title']; ?>" data-paroller-factor="0.05" data-aos="fade-up" data-aos-duration="1500">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-5"></div>
						<div class="col-lg-7 nopadding">
							<div class="text" data-aos="fade-left" data-aos-duration="1500">
								<h3><?php echo $proces['title']; ?></h3>
								<?php echo $proces['text']; ?>
							</div>
						</div>
					</div>
				</div>
				<?php } elseif ( $i == $array_size - 1 ) {  ?>
				<div class="process__row image__text">
					<div class="row">
						<div class="col-lg-7">
							<div class="image paroller">
								<img src="<?php echo $proces['image']['url']; ?>" title="<?php echo $proces['image']['title']; ?>" data-paroller-factor="0.05" data-aos="fade-up" data-aos-duration="1500">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4"></div>
						<div class="col-lg-8">
							<div class="text" data-aos="fade-left" data-aos-duration="1500">
								<h3><?php echo $proces['title']; ?></h3>
								<?php echo $proces['text']; ?>
							</div>
						</div>
					</div>
				</div>
				<?php }
			$i++; } ?>
			</div>
		</div>
		<?php } ?>
	</div>
</section>