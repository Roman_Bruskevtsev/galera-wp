<section class="services__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-6">
				<div class="section__title" data-aos="fade-right" data-aos-duration="1500">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
		<?php } 
		$services = get_sub_field('services'); 
		if( $services ) { ?>
		<div class="row">
			<div class="col-lg-12 services">
			<?php 
			$top = 100;
			foreach ( $services as $service ) { ?>
				<div class="service" data-top="<?php echo $top; ?>">
					<div class="name">
						<?php if( $service['title'] ) { ?><h3><?php echo $service['title']; ?></h3><?php } ?>
						<div class="content">
							<?php if( $service['text'] ) { ?><p><?php echo $service['text'] ?></p><?php } ?>
							<?php if( $service['button_link'] ) { ?><a class="btn btn__gradient" href="<?php echo $service['button_link'] ?>"><span><?php echo $service['button_label'] ?></span></a><?php } ?>
						</div>
					</div>
					<?php 
					$video = $service['video_url']; 
					if( $video ) { ?>
					<div class="video">
						<video autoplay muted loop preload playsinline>
							<source src="<?php echo $video; ?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
						</video>
					</div>
					<?php } ?>
				</div>
			<?php 
			$top = $top + 100; 
			} ?>
			</div>
		</div>
		<?php } ?>
	</div>
</section>