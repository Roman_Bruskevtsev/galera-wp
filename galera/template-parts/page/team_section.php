<section class="team__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-6">
				<div class="section__title" data-aos="fade-right" data-aos-duration="1500">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<div class="container team__block d-none d-lg-block">
		<?php 
		$image = get_sub_field('team_image'); 
		$team = get_sub_field('team'); 
		if( $team ) { ?>
		<div class="row justify-content-start">
			<div class="col-md-7">
				<div class="images" data-aos="fade-up" data-aos-duration="1500">
					<?php if( $image ) { ?>
					<div class="default__image">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
					</div>
					<?php } ?>
					<div class="team__list">
						<?php foreach ($team as $person) { 
							$image = $person['image']; 
							if( $image ) { ?>
							<div class="image">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
							</div>
							<?php }
						} ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row justify-content-end">
			<div class="col-lg-6">
				<div class="team__information">
				<?php foreach ($team as $person) { ?>
					<div class="person cursor__hover" data-aos="fade-left" data-aos-duration="1500" onmouseover="galera.showTeamPerson(this);" onmouseout="galera.hideTeamPerson(this);">
						<?php if($person['name']) { ?><h3 data-name="<?php echo $person['name']; ?>"><?php echo $person['name']; ?></h3><?php } ?>
						<?php if($person['position']) { ?><p><?php echo $person['position']; ?></p><?php } ?>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<?php if( $team ) { ?>
	<div class="container nopadding d-block d-lg-none">
		<div class="row">
			<div class="col">
				<div class="team__slider">
				<?php foreach ($team as $person) { ?>
					<div class="slide">
						<?php $image = $person['image']; 
						if( $image ) { ?>
						<div class="image">
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
						</div>
						<div class="person text-center">
							<?php if($person['name']) { ?><h3 data-name="<?php echo $person['name']; ?>"><?php echo $person['name']; ?></h3><?php } ?>
							<?php if($person['position']) { ?><p><?php echo $person['position']; ?></p><?php } ?>
						</div>
						<?php } ?>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
</section>