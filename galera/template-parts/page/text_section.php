<?php if( get_sub_field('content') ) { ?>
<section class="content__section">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="text" data-aos="fade-up" data-aos-duration="1000"><?php the_sub_field('content'); ?></div>
			</div>
		</div>
	</div>
</section>
<?php } ?>