<section class="use__cases__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="title" data-aos="fade-right" data-aos-duration="1500">
					<h3><?php the_sub_field('title'); ?></h3>
				</div>
			</div>
		</div>
		<?php }
		$cases = get_sub_field('cases'); 
		if( $cases ) { ?>
		<div class="row">
			<div class="col-lg-12">
			<?php foreach ( $cases as $case ) { ?>
				<div class="case" data-aos="fade-up" data-aos-duration="600">
					<div class="icon">
						<?php if( $case['icon'] ) { ?><img src="<?php echo $case['icon']['url']; ?>" alt="<?php echo $case['title']; ?>"><?php } ?>
					</div>
					<div class="text">
						<?php if( $case['title'] ) { ?><h4><b><?php echo $case['title']; ?></b></h4><?php } ?>
					</div>
				</div>
			<?php } ?>
			</div>
		</div>
		<?php } ?>
	</div>
</section>