<section class="video__vimeo__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="section__title" data-aos="fade-right" data-aos-duration="1500">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
		<?php $video = get_sub_field('video_id_vimeo'); 
		if( $video ) {
			$background = get_sub_field('video_preview_image') ? ' style="background-image: url('.get_sub_field('video_preview_image').')"' : ' style="background-color: #424242;"'; ?>
		<div class="row">
			<div class="col">
				<div class="video__block" data-aos="fade-up" data-aos-duration="1000">
					<div class="preview"<?php echo $background; ?>></div>
					<div class="play"></div>
					<iframe frameborder="0" data-src="https://player.vimeo.com/video/<?php echo $video; ?>" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>