<?php if( get_sub_field('image_before') && get_sub_field('image_after') ) { ?>
<section class="before__after__section">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="images cocoen">
					<img src="<?php echo get_sub_field('image_before')['url']; ?>" alt="<?php echo get_sub_field('image_before')['title']; ?>">
					<img src="<?php echo get_sub_field('image_after')['url']; ?>" alt="<?php echo get_sub_field('image_after')['title']; ?>">
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>