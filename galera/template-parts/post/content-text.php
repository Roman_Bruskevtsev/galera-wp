<section class="post__text">
	<div class="container">
		<div class="row">
			<div class="col-lg-12"><?php the_content(); ?></div>
		</div>
	</div>
</section>