<?php 
$blog_id = get_option('page_for_posts');
$thumbnail = get_the_post_thumbnail( get_the_ID() , 'full' ) ? 
			 get_the_post_thumbnail( get_the_ID() , 'full' ) : ''
?>
<section class="post__title">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php if( $blog_id ) { ?>
				<div class="post__nav" data-aos="fade-left">
					<a href="<?php echo get_permalink( $blog_id ); ?>"><?php _e('Back', 'zahbug'); ?></a>
				</div>
				<?php } ?>
				<div class="post__banner" data-aos="fade-up" data-aos-duration="600">
					<?php echo $thumbnail; ?>
					<h1><?php the_title(); ?></h1>
					<div class="data"><?php echo get_the_date(); ?></div>
				</div>
			</div>
		</div>
	</div>
</section>