<section class="images__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
			<?php if( get_sub_field('image_1') ) { ?>
				<div class="image">
					<img src="<?php echo get_sub_field('image_1')['url']; ?>" alt="<?php echo get_sub_field('image_1')['title']; ?>">
				</div>
			<?php } ?>
			</div>
			<div class="col-lg-6">
			<?php if( get_sub_field('image_2') ) { ?>
				<div class="image">
					<img src="<?php echo get_sub_field('image_2')['url']; ?>" alt="<?php echo get_sub_field('image_2')['title']; ?>">
				</div>
			<?php } ?>
			</div>
		</div>
	</div>
</section>