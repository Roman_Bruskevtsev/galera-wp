<?php 
$video = get_sub_field('video_id_vimeo'); 
$preview = get_sub_field('preview');
if( $video ) { 
	$background = $preview ? ' style="background-image: url('.$preview['url'].')"' : '';
	?>
<section class="video__section">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="video__block">
					<div class="preview"<?php echo $background; ?>></div>
					<div class="play"></div>
					<iframe frameborder="0" data-src="https://player.vimeo.com/video/<?php echo $video; ?>" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>