<?php 
$slider = get_sub_field('videos');
?>
<section class="video__slider__section">
	<?php if( get_sub_field('title') ) { ?>
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="section__title">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	<div class="container-fluid">
		<?php if( $slider ) { ?>
		<div class="row">
			<div class="col nopadding">
				<div class="video__slider">
				<?php foreach ($slider as $slide) { 
					$preview = $slide['preview_image'] ? ' style="background-image:url('.$slide['preview_image']['url'].')"' : ''; ?>
					<div class="slide">
						<div class="preview"<?php echo $preview; ?>>
							<?php if( $slide['description'] ) { ?><div class="text"><?php echo $slide['description']; ?></div><?php } ?>
						</div>
						<?php $video = $slide['video_id_vimeo']; 
						if( $video ) { ?>
						<button class="play__btn" onclick="galera.playVideo('<?php echo 'https://player.vimeo.com/video/'.$video; ?>', this);"></button>
						<?php } ?>
					</div>
				<?php } ?>
				</div>
				<div class="video__popup">
					<div class="wrapper"><span class="close" onclick="galera.closeVideo(this);"></span></div>
					<div class="popup">
						<div class="video"></div>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>