<?php 
$thumbnail = get_the_post_thumbnail_url( get_the_ID() , 'post-thumbnail-wide' ) ? 
			 ' style="background-image:url('.get_the_post_thumbnail_url( get_the_ID() , 'post-thumbnail-wide' ).')"' : ''
?>
<div class="col-lg-12">
	<a href="<?php the_permalink(); ?>" class="post__block"<?php echo $thumbnail; ?>>
		<div class="content short">
			<div class="text"><h3><?php the_title(); ?></h3></div>
		</div>
		<div class="content full">
			<div class="text">
				<h3><?php the_title(); ?></h3>
				<?php the_excerpt(); ?>
				<div class="date"><?php echo get_the_date(); ?></div>
				<div class="button__row text-right">
					<button class="btn btn__white"><span><?php _e('Read', 'galera'); ?></span></button>
				</div>
			</div>
		</div>
	</a>
</div>