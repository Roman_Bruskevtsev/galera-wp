<?php if( get_sub_field('image') ) { ?>
<div class="full__image">
	<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>">
</div>
<?php } ?>