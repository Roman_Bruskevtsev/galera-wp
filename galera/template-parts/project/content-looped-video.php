<?php if( get_sub_field('video_url') ) { ?>
<div class="looped__video">
	<video autoplay muted loop preload playsinline>
		<source src="<?php echo get_sub_field('video_url'); ?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
	</video>
</div>
<?php } ?>