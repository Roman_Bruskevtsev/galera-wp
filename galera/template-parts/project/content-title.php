<div class="title">
	<div class="subtitle">
		<?php if( get_field('subtitle') ) { ?><h6><?php the_field('subtitle'); ?></h6><?php } ?>
		<span class="date"><?php echo get_the_date(); ?></span>
	</div>
	<h2 class="h3"><?php the_title(); ?></h2>
</div>