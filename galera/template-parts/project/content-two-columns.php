<div class="two__columns">
	<?php if( get_sub_field('title') ) { ?>
	<div class="title">
		<h4><b><?php the_sub_field('title'); ?></b></h4>
	</div>
	<?php } 
	$images = get_sub_field('images'); 
	if( $images ) { ?>
	<div class="images">
		<div class="row">
		<?php foreach ( $images as $image ) { ?>
			<div class="col-lg-6">
				<div class="image">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
				</div>
			</div>
		<?php } ?>
		</div>
	</div>
	<?php } ?>
</div>