<?php if( get_sub_field('video_id_vimeo') ){ ?>
	<iframe frameborder="0" data-src="https://player.vimeo.com/video/<?php the_sub_field('video_id_vimeo'); ?>" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<?php } ?>