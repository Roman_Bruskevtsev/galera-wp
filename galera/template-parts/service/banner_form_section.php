<section class="banner__form__section">
	<div class="container">
		<?php if( get_sub_field('image') ) { ?>
		<div class="row">
			<div class="col-lg-7"></div>
			<div class="col-lg-5">
				<div class="image" data-aos="fade-left" data-aos-delay="300" data-aos-duration="1000"><img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>"></div>
			</div>
		</div>
		<?php } ?>
		<div class="row">
			<div class="col-lg-8">
				<div class="content" data-aos="fade-up" data-aos-delay="300" data-aos-duration="600">
					<?php if( get_sub_field('title') ) { ?>
						<h1 class="h5"><?php the_sub_field('title'); ?></h1>
					<?php } 
					if( get_sub_field('subtitle') ) { ?>
						<h2 class="h1"><?php the_sub_field('subtitle'); ?></h2>
					<?php } ?>
					<div class="block">
						<?php the_sub_field('text'); ?>
						<form class="validation__form inline__form" autocomplete="off" action="<?php echo get_the_permalink(); ?>"  onsubmit="return formClass.validation(this);" name="contact-form" method="post" accept-charset="utf-8" novalidate>
							<?php wp_nonce_field('security', 'form-security_'.wp_unique_id()); ?>
							<input type="text" class="form__field" name="subject" value="<?php _e('Contact form', 'galera'); ?>" hidden readonly required>
							<div class="field__group">
								<label>
									<input type="email" name="email" class="form__field" placeholder="<?php _e('E-mail', 'bca'); ?>" required onkeyup="formClass.checkField(this);">
								</label>
							</div>
							<div class="field__group">
								<label>
									<input type="text" name="name" class="form__field" placeholder="<?php _e('Name', 'bca'); ?>" required onkeyup="formClass.checkField(this);">
								</label>
							</div>
							<div class="submit__group">
								<button class="btn btn__gradient" type="submit"><span><?php _e('Get a quote', 'galera'); ?></span></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>