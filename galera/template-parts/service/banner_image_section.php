<?php 
$image = get_sub_field('image') ? ' style="background-image: url('.get_sub_field('image').')"' : '';
?>
<section class="banner__image__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="banner"<?php echo $image; ?>>
					<div class="row justify-content-center">
						<div class="col-lg-8">
							<div class="text text-center" data-aos="fade-up" data-aos-duration="600">
								<?php if( get_sub_field('title') ) { ?><h3><?php the_sub_field('title'); ?></h3><?php } ?>
								<a href="<?php the_sub_field('button_url'); ?>" class="btn btn__gradient"><span><?php the_sub_field('button_text'); ?></span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>