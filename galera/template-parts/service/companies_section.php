<section class="companies__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-7">
				<div class="title" data-aos="fade-right" data-aos-duration="1500">
					<h3><?php the_sub_field('title'); ?></h3>
				</div>
			</div>
		</div>
		<?php } 
		$companies = get_sub_field('logos'); 
		if( $companies ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="companies">
				<?php foreach ( $companies as $company ) { ?>
					<div class="company" data-aos="fade-up" data-aos-duration="600">
						<img src="<?php echo $company['url']; ?>" alt="<?php echo $company['title']; ?>">
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>
