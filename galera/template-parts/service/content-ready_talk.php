<?php
$section = get_field('services', 'option')['ready_to_talk_section']; 
?>
<section class="ready__talk__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="content">
					<div class="ready__text">
						<div class="row">
							<div class="col-lg-6 order-last">
								<div class="person">
									<?php if( $section['person_avatar'] ) { ?>
									<img src="<?php echo $section['person_avatar']['url']; ?>" alt="<?php echo $section['person_avatar']['title']; ?>">
									<?php } ?>
									<div class="person__info">
										<?php if( $section['name'] ) { ?><h4><?php echo $section['name']; ?></h4><?php } ?>
										<?php if( $section['position'] ) { ?><p><?php echo $section['position']; ?></p><?php } ?>
									</div>
								</div>
							</div>
							<div class="col-lg-6 order-first">
								<div class="text">
									<?php if( $section['title'] ) { ?><h2><?php echo $section['title']; ?></h2><?php } ?>
									<?php if( $section['button_link'] ) { ?><a class="btn btn__gradient" href="<?php echo $section['button_link']; ?>"><span><?php echo $section['button_text']; ?></span></a><?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>