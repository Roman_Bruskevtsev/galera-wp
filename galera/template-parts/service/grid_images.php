<section class="grid__images">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="title" data-aos="fade-left" data-aos-duration="600">
					<h3><?php the_sub_field('title'); ?></h3>
				</div>
			</div>
		</div>
		<?php } 
		$image_1 = get_sub_field('image_1');
		$image_2 = get_sub_field('image_2');
		$image_3 = get_sub_field('image_3'); 
		if( $image_1 || $image_2 || $image_3 ) { ?>
		<div class="row">
		<?php if( $image_1 ) { ?>
			<div class="col-lg-12">
				<div class="image" data-aos="fade-up" data-aos-duration="600">
					<img src="<?php echo $image_1['url']; ?>" alt="<?php echo $image_1['title']; ?>">
				</div>
			</div>
		<?php } ?>
		<?php if( $image_2 ) { ?>
			<div class="col-lg-4">
				<div class="image" data-aos="fade-right" data-aos-duration="600">
					<img src="<?php echo $image_2['url']; ?>" alt="<?php echo $image_2['title']; ?>">
				</div>
			</div>
		<?php } ?>
		<?php if( $image_3 ) { ?>
			<div class="col-lg-8">
				<div class="image" data-aos="fade-left" data-aos-duration="600">
					<img src="<?php echo $image_3['url']; ?>" alt="<?php echo $image_3['title']; ?>">
				</div>
			</div>
		<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>