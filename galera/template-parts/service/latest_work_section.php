<section class="latest__work__section">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="title" data-aos="fade-right" data-aos-duration="1500">
					<h3><?php the_sub_field('title'); ?></h3>
				</div>
			</div>
		</div>
		<?php } 
		$projects = get_sub_field('projects'); 
		$args = array(
			'posts_per_page' => -1,
			'orderby' 		 => 'post__in',
			'post_type'		 => 'project',
			'post__in'		 => $projects
		);

		$query = new WP_Query( $args );

		if ( $query->have_posts() ) {
			$i = 0; ?>
			<div class="row">
			<?php while ( $query->have_posts() ) { $query->the_post();
			$thumbnail = get_the_post_thumbnail_url( get_the_ID() , 'post-thumbnail' ) ? 
			 ' style="background-image:url('.get_the_post_thumbnail_url( get_the_ID() , 'post-thumbnail' ).')"' : '';
			if( $i == 0 ) { ?>
				<div class="col-lg-7">
					<div class="project__block large"<?php echo $thumbnail; ?> data-id="<?php echo $i; ?>" data-aos="fade-right" data-aos-duration="1000">
						<div class="content">
							<div class="details">
								<?php if( get_field('subtitle') ) { ?>
									<span class="subtitle float-left"><?php the_field('subtitle'); ?></span>
								<?php } ?>
								<span class="date float-right"><?php echo get_the_date(); ?></span>
							</div>
							<h4><b><?php the_title(); ?></b></h4>
							<div class="watch__link"><?php _e('Watch', 'galera'); ?></div>
						</div>
					</div>
				</div>
			<?php } elseif( $i == 7 ) { ?>
				<div class="col-lg-7">
					<div class="project__block large"<?php echo $thumbnail; ?> data-id="<?php echo $i; ?>" data-aos="fade-left" data-aos-duration="1000">
						<div class="content">
							<div class="details">
								<?php if( get_field('subtitle') ) { ?>
									<span class="subtitle float-left"><?php the_field('subtitle'); ?></span>
								<?php } ?>
								<span class="date float-right"><?php echo get_the_date(); ?></span>
							</div>
							<h4><b><?php the_title(); ?></b></h4>
							<div class="watch__link"><?php _e('Watch', 'galera'); ?></div>
						</div>
					</div>
				</div>
			<?php } elseif( $i == 1 || $i == 4 ) { ?>
				<div class="col-lg-5">
					<div class="project__block medium"<?php echo $thumbnail; ?> data-id="<?php echo $i; ?>" data-aos="fade-up" data-aos-duration="1000">
						<div class="content">
							<div class="details">
								<?php if( get_field('subtitle') ) { ?>
									<span class="subtitle float-left"><?php the_field('subtitle'); ?></span>
								<?php } ?>
								<span class="date float-right"><?php echo get_the_date(); ?></span>
							</div>
							<h4><b><?php the_title(); ?></b></h4>
							<div class="watch__link"><?php _e('Watch', 'galera'); ?></div>
						</div>
					</div>
				
			<?php } elseif( $i == 2 || $i == 5 ) { ?>
					<div class="row">
						<div class="col-lg-6">
							<div class="project__block small"<?php echo $thumbnail; ?> data-id="<?php echo $i; ?>" data-aos="fade-up" data-aos-duration="1000">
								<div class="content">
									<div class="details">
										<?php if( get_field('subtitle') ) { ?>
											<span class="subtitle float-left"><?php the_field('subtitle'); ?></span>
										<?php } ?>
									</div>
									<h4><b><?php the_title(); ?></b></h4>
									<div class="watch__link"><?php _e('Watch', 'galera'); ?></div>
								</div>
							</div>
						</div>
			<?php } elseif( $i == 3 || $i == 6) { ?>
						<div class="col-lg-6">
							<div class="project__block small"<?php echo $thumbnail; ?> data-id="<?php echo $i; ?>" data-aos="fade-up" data-aos-duration="1000">
								<div class="content">
									<div class="details">
										<?php if( get_field('subtitle') ) { ?>
											<span class="subtitle float-left"><?php the_field('subtitle'); ?></span>
										<?php } ?>
									</div>
									<h4><b><?php the_title(); ?></b></h4>
									<div class="watch__link"><?php _e('Watch', 'galera'); ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } $i++; } ?>
			</div>
		<?php } wp_reset_postdata(); ?>
	</div>
</section>
<?php if ( $query->have_posts() ) { ?>
<section class="latest__popups">
	<?php while ( $query->have_posts() ) { $query->the_post(); ?>
	<div class="popup">
		<div class="content">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-12">
						<span class="close"></span>
						<?php 
						get_template_part( 'template-parts/project/content', 'title' ); 
						get_template_part( 'template-parts/project/content', 'video' );

						if( have_rows('content') ):
							while ( have_rows('content') ) : the_row();
								if( get_row_layout() == 'vimeo_video' ) :
									get_template_part( 'template-parts/project/content', 'video' );
								elseif( get_row_layout() == 'looped_video' ) :
									get_template_part( 'template-parts/project/content', 'looped-video' );
								elseif( get_row_layout() == 'full_image' ) :
									get_template_part( 'template-parts/project/content', 'full-image' );
								elseif( get_row_layout() == 'process_images_two_columns' ) :
									get_template_part( 'template-parts/project/content', 'two-columns' );
								elseif( get_row_layout() == 'process_images_one_column' ) :
									get_template_part( 'template-parts/project/content', 'one-column' );
								elseif( get_row_layout() == 'description' ) :
									get_template_part( 'template-parts/project/content', 'description' );
								endif;
							endwhile;
						endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
</section>
<?php } wp_reset_postdata(); ?>