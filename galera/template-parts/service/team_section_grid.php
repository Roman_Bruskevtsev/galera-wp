<section class="team__section__grid">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="title" data-aos="fade-right" data-aos-duration="1500">
					<h3><?php the_sub_field('title'); ?></h3>
				</div>
			</div>
		</div>
		<?php } 
		$team = get_sub_field('team'); 
		if( $team ) { ?>
		<div class="row">
			<?php foreach ($team as $person) { 
			$avatar = $person['avatar'] ? ' style="background-image: url('.$person['avatar']['url'].')"' : ''; ?>
			<div class="col-lg-4">
				<div class="person" data-aos="fade-up" data-aos-duration="600">
					<div class="avatar"<?php echo $avatar; ?>></div>
					<?php if( $person['name'] ) { ?><h4><b><?php echo $person['name']; ?></b></h4><?php } ?>
					<?php if( $person['position'] ) { ?><p><?php echo $person['position']; ?></p><?php } ?>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</section>