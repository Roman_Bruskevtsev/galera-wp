<?php
/**
 * Template Name: Success page
 *
 * @package WordPress
 * @subpackage Galera
 * @since 1.0
 * @version 1.0
 */
get_header(); 

get_template_part( 'template-parts/page/content', 'title' );
?>

<section class="success__page">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="content text-center" data-aos="fade-up" data-aos-duration="1500">
					<?php if( get_field('title') ) { ?><h1 class="text-center"><?php the_field('title'); ?></h1><?php } ?>
					<?php if( get_field('subtitle') ) { ?><h3 class="text-center"><?php the_field('subtitle'); ?></h3><?php } ?>
				</div>
				<div class="form__block">
					<?php 
					if( get_field('email') || get_field('phone') || get_field('address') ) { ?>
					<div class="row">
						<div class="col">
						<?php if( get_field('email') ) { ?>
							<div class="contact__block email" data-aos="fade-up" data-aos-duration="1500">
								<h4><b><?php _e('Email', 'galera'); ?></b></h4>
								<a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a>
							</div>
						<?php } 
						if( get_field('phone') ) { ?>
							<div class="contact__block phone" data-aos="fade-up" data-aos-duration="1500">
								<h4><b><?php _e('Phone', 'galera'); ?></b></h4>
								<a href="tel:<?php the_field('phone'); ?>"><?php the_field('phone'); ?></a>
							</div>
						<?php } 
						if( get_field('address') ) { ?>
							<div class="contact__block address" data-aos="fade-up" data-aos-duration="1500">
								<h4><b><?php _e('Address', 'galera'); ?></b></h4>
								<a href="https://www.google.com/maps/place/<?php the_field('address'); ?>" target="_blank"><?php the_field('address'); ?></a>
							</div>
						<?php } ?>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();