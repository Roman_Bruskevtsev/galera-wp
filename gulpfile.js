'use strict';
var themeName           = 'galera',
    gulp                = require('gulp'),
    watch               = require('gulp-watch'),
    sass                = require('gulp-sass'),
    cssmin              = require('gulp-cssmin'),
    babel               = require('gulp-babel'),
    uglify              = require('gulp-uglify-es').default,
    rename              = require('gulp-rename'),
    concat              = require('gulp-concat'),
    pipeline            = require('readable-stream').pipeline,
    buildFolder         = themeName,
    scriptsArray        = [
        buildFolder + '/assets/js/slick.js',
        buildFolder + '/assets/js/aos.js',
        buildFolder + '/assets/js/jquery.paroller.js',
        buildFolder + '/assets/js/parallax.js',
        buildFolder + '/assets/js/select2.js',
        buildFolder + '/assets/js/cocoen.js',
        buildFolder + '/assets/js/main.js'
    ],
    classScriptsArray   = [
        buildFolder + '/inc/classes/assets/js/form.js',
        buildFolder + '/inc/classes/assets/js/general.js'
    ];

gulp.task('styles',  function () {
    return gulp.src(buildFolder + '/assets/sass/**/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest(buildFolder + '/assets/css'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(buildFolder + '/assets/css/'));    
});

gulp.task('scripts', function () {
    return gulp.src(scriptsArray).pipe(concat('all.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(buildFolder + '/assets/js/'));
});

gulp.task('classes-scripts', function () {
    return gulp.src(classScriptsArray).pipe(concat('classes.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(buildFolder + '/inc/classes/assets/js/'));
});

gulp.task('default', gulp.series(
    gulp.parallel(
    'styles',
    'scripts',
    'classes-scripts'
    )
));

gulp.task('watch', function () {
    gulp.watch(buildFolder + '/assets/sass/**/*.scss', gulp.series('styles'));

    gulp.watch(scriptsArray, gulp.series('scripts'));

    gulp.watch(classScriptsArray, gulp.series('classes-scripts'));
});